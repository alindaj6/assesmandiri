//
//  HomeViewController.swift
//  AssesMandiri
//
//  Created by Angga Setiawan on 09/02/24.
//

import UIKit

protocol HomeViewInterface: AnyObject {
    func setupView()
    func setTitle(with title: String)
    func showView()
}

class HomeViewController: UIViewController {

    fileprivate lazy var homeTableView: UITableView = {
        let tableView = UITableView()
        tableView.allowsSelection = true
        tableView.delegate = self
        tableView.dataSource = self
        tableView.rowHeight = UITableView.automaticDimension
        tableView.showsVerticalScrollIndicator = false
        tableView.backgroundColor = .white
        tableView.isHidden = false
        return tableView
    }()

    var presenter: HomePresenterInterface = HomePresenter(view: nil, router: nil, interactor: nil)

    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.notifyViewDidLoad()
    }
}

// MARK: - Interface Setup
extension HomeViewController: HomeViewInterface {

    func setupView() {
        self.view.backgroundColor = .white
        self.addView()
    }

    func setTitle(with title: String) {
        self.title = title
    }

    func showView() {
        self.homeTableView.reloadData()
    }
}

extension HomeViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter.getItemCount
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        let row = indexPath.row
        cell.textLabel?.text = presenter.getGenreBy(row)?.name ?? ""
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let row = indexPath.row
        if let genre = presenter.getGenreBy(row) {
            presenter.goToDiscover(genre: genre)
        }
    }
}

extension HomeViewController {
    fileprivate func addView() {
        self.view.addSubview(homeTableView)
        homeTableView.snp.makeConstraints { make in
            make.top.equalTo(self.view.safeAreaLayoutGuide.snp.top)
            make.bottom.equalTo(self.view.safeAreaLayoutGuide.snp.bottom)
            make.leading.trailing.equalToSuperview()
        }
    }
}
