//
//  HomePresenter.swift
//  AssesMandiri
//
//  Created by Angga Setiawan on 09/02/24.
//

import Foundation

protocol HomePresenterInterface: AnyObject {
    func notifyViewDidLoad()
    func getGenreBy(_ row: Int) -> MovieGenre?
    func fetchDatas()
    var getItemCount: Int { get }
    func goToDiscover(genre: MovieGenre)
}

final class HomePresenter {

    private weak var view: HomeViewInterface?
    private var router: HomeRouterInterface?
    private var interactor: HomeInteractorInterface?
    private var genres: [MovieGenre] = []

    init(view: HomeViewInterface?, router: HomeRouterInterface?, interactor: HomeInteractorInterface?) {
        self.view = view
        self.router = router
        self.interactor = interactor
    }
}

// MARK: - Interface Setup
extension HomePresenter: HomePresenterInterface {

    func notifyViewDidLoad() {
        view?.setupView()
        view?.setTitle(with: "Home")
        fetchDatas()
    }

    func getGenreBy(_ row: Int) -> MovieGenre? {
        if genres.isEmpty { return nil }
        return genres[row]
    }

    func fetchDatas() {
        Task { @MainActor in
            self.genres = await interactor?.getGenres() ?? []
            view?.showView()
        }
    }

    var getItemCount: Int {
        genres.count
    }

    func goToDiscover(genre: MovieGenre) {
        router?.navigateToDiscover(from: view!, genre: genre)
    }
}
