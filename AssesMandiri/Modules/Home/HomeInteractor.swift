//
//  HomeInteractor.swift
//  AssesMandiri
//
//  Created by Angga Setiawan on 09/02/24.
//

import Foundation

protocol HomeInteractorInterface: AnyObject {
    func getGenres() async -> [MovieGenre]
}

final class HomeInteractor: HomeInteractorInterface {

    func getGenres() async -> [MovieGenre] {
        do {
            let movieGenres = try await APIService.request(for: Genres.self, route: MovieDBApi.getMovieGenres)
            return movieGenres.genres
        } catch {
            print("ERROR: \(error.localizedDescription)")
            return []
        }
    }
}
