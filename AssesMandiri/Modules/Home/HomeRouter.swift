//
//  HomeRouter.swift
//  AssesMandiri
//
//  Created by Angga Setiawan on 09/02/24.
//

import UIKit

protocol HomeRouterInterface: AnyObject {
    func navigateToDiscover(from view: HomeViewInterface, genre: MovieGenre)
}

final class HomeRouter: HomeRouterInterface {

    static func createModule() -> UINavigationController {
        let router = HomeRouter()
        let viewController = HomeViewController()
        let interactor = HomeInteractor()
        let presenter = HomePresenter(view: viewController, router: router, interactor: interactor)

        viewController.presenter = presenter
        return UINavigationController(rootViewController: viewController)
    }

    func navigateToDiscover(from view: HomeViewInterface, genre: MovieGenre) {
        let destination = DiscoverRouter.createModule(with: genre)
        if let sourceView = view as? UIViewController {
            sourceView.navigationController?.pushViewController(destination, animated: true)
        }
    }
}
