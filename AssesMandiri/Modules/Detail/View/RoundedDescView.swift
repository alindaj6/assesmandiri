//
//  RoundedDescView.swift
//  AssesMandiri
//
//  Created by Angga Setiawan on 09/02/24.
//

import UIKit

class RoundedDescView: UIView {

    func setupUI(imgURL: URL?, desc: String) {
        self.imgView.setImage(with: imgURL, placeholder: .custom(image: UIImage(named: "user")!))
        self.descLabel.text = desc
    }

    fileprivate lazy var imgView: UIImageView = {
        let imgView = UIImageView()
        imgView.contentMode = .scaleAspectFill
        imgView.image = UIImage()
        imgView.clipsToBounds = true
        imgView.layer.cornerRadius = 60
        return imgView
    }()

    fileprivate lazy var descLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.font = .systemFont(ofSize: 12)
        label.textAlignment = .center
        return label
    }()

    override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = .clear
        self.addView()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension RoundedDescView {
    fileprivate func addView() {
        self.addSubview(imgView)
        self.addSubview(descLabel)

        imgView.snp.makeConstraints { make in
            make.top.leading.trailing.equalToSuperview()
            make.size.equalTo(120)
        }

        descLabel.snp.makeConstraints { make in
            make.top.equalTo(imgView.snp.bottom).offset(8)
            make.leading.trailing.equalTo(imgView)
            make.bottom.lessThanOrEqualToSuperview().inset(10)
        }
    }
}
