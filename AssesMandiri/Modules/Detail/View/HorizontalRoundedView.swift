//
//  HorizontalRoundedView.swift
//  AssesMandiri
//
//  Created by Angga Setiawan on 09/02/24.
//

import UIKit

public protocol MovieCreditModel {
    var imgProfileUrl: URL? { get }
    var desc: String { get }
}

extension MovieCredit: MovieCreditModel {
    public var imgProfileUrl: URL? {
        return URL(string: "https://image.tmdb.org/t/p/original/\(profilePath)")
    }

    public var desc: String {
        if !character.isEmpty {
            return "\(name)\nas\n\(character)"
        } else {
            return "\(name)\nas\n\(job)"
        }
    }
}

class HorizontalRoundedView: UIView {

    func setupUI(title: String, views: [UIView]) {
        self.titleLabel.text = title

        if !infoStackView.arrangedSubviews.isEmpty {
            infoStackView.removeAllArrangedSubviews()
        }

        for view in views {
            infoStackView.addArrangedSubview(view)
        }
    }

    func setupUI(title: String, movieCredits: [MovieCredit]) {
        self.titleLabel.text = title

        if !infoStackView.arrangedSubviews.isEmpty {
            infoStackView.removeAllArrangedSubviews()
        }

        for credit in movieCredits {
            let view = RoundedDescView()
            view.setupUI(imgURL: credit.imgProfileUrl, desc: credit.desc)
            infoStackView.addArrangedSubview(view)
        }
    }

    fileprivate lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.text = ""
        label.font = .boldSystemFont(ofSize: 18)
        label.numberOfLines = 1
        label.adjustsFontSizeToFitWidth = true
        label.minimumScaleFactor = 0.3
        return label
    }()

    fileprivate lazy var scrollView: UIScrollView = {
        let scrollView = UIScrollView()
        scrollView.showsHorizontalScrollIndicator = false
        return scrollView
    }()

    fileprivate lazy var infoStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .horizontal
        stackView.distribution = .equalSpacing
        stackView.spacing = 8
        return stackView
    }()

    override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = .clear
        self.addView()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension HorizontalRoundedView {
    fileprivate func addView() {
        self.addSubview(titleLabel)
        self.addSubview(scrollView)

        scrollView.addSubview(infoStackView)

        titleLabel.snp.makeConstraints { make in
            make.top.equalToSuperview()
            make.leading.trailing.equalToSuperview().inset(8)
        }

        scrollView.snp.makeConstraints { make in
            make.top.equalTo(titleLabel.snp.bottom).offset(6)
            make.leading.trailing.height.equalToSuperview()
            make.height.equalTo(210)
        }

        infoStackView.snp.makeConstraints { make in
            make.top.bottom.equalTo(scrollView)
            make.leading.trailing.equalTo(scrollView).inset(8)
        }
    }
}
