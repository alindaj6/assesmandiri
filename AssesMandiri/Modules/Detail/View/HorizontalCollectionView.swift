//
//  HorizontalCollectionView.swift
//  AssesMandiri
//
//  Created by Angga Setiawan on 09/02/24.
//

import UIKit

protocol HorizontalCollectionDelegate: AnyObject {
    func didViewAllTapped()
}

class HorizontalCollectionView: UIView {

    fileprivate var movies: [MovieView] = [] {
        didSet {
            self.delay(interval: .milliseconds(500)) {
                self.movieCollectionView.reloadData()
            }
        }
    }

    fileprivate var reviews: [MovieReview] = [] {
        didSet {
            self.delay(interval: .milliseconds(500)) {
                self.movieCollectionView.reloadData()
            }
        }
    }

    func setupUI(title: String, movies: [MovieView]) {
        self.titleLabel.text = title
        self.movies = movies
    }

    func setupUI(title: String, reviews: [MovieReview], delegate: HorizontalCollectionDelegate?) {
        self.delegate = delegate

        viewAllLabel.isHidden = false
        viewAllLabel.addGestureRecognizer(UITapGestureRecognizer(
            target: self, action: #selector(viewAllTapped)))

        self.height = 128
        movieCollectionView.snp.remakeConstraints { make in
            make.top.equalTo(titleLabel.snp.bottom).offset(16)
            make.leading.trailing.equalTo(containerView)
            make.bottom.equalTo(containerView).inset(16)
            make.height.equalTo(self.height)
        }

        self.titleLabel.text = title
        self.reviews = reviews
    }

    fileprivate lazy var containerView: UIView = {
        let view = UIView()
        view.backgroundColor = .clear
        return view
    }()

    fileprivate lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.text = ""
        label.font = .boldSystemFont(ofSize: 18)
        label.numberOfLines = 1
        label.adjustsFontSizeToFitWidth = true
        label.minimumScaleFactor = 0.3
        return label
    }()

    fileprivate lazy var viewAllLabel: UILabel = {
        let label = UILabel()
        label.text = "See All"
        label.textAlignment = .right
        label.numberOfLines = 1
        label.adjustsFontSizeToFitWidth = true
        label.minimumScaleFactor = 0.3
        label.isUserInteractionEnabled = true
        label.isHidden = true
        return label
    }()

    fileprivate lazy var movieCollectionView: UICollectionView = {
        let collectView = UICollectionView(frame: .zero, collectionViewLayout: UICollectionViewFlowLayout())
        collectView.alpha = 1.0
        return collectView
    }()

    fileprivate weak var delegate: HorizontalCollectionDelegate?
    fileprivate var height: CGFloat = 350

    override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = .clear
        self.addView()
        self.initCollectionView()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    @objc func viewAllTapped() {
        if !reviews.isEmpty {
            delegate?.didViewAllTapped()
        }
    }
}

extension HorizontalCollectionView: UICollectionViewDelegate, UICollectionViewDataSource,
            UICollectionViewDelegateFlowLayout, UIScrollViewDelegate {

    fileprivate func initCollectionView() {
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        self.movieCollectionView.setCollectionViewLayout(layout, animated: true)
        self.movieCollectionView.register(HorizontalCollectionCell.self,
                                         forCellWithReuseIdentifier: HorizontalCollectionCell.identifier())
        self.movieCollectionView.showsHorizontalScrollIndicator = false
        self.movieCollectionView.backgroundColor = .clear
        self.movieCollectionView.delegate = self
        self.movieCollectionView.dataSource = self
    }

    // Check how much data
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return !self.movies.isEmpty ? self.movies.count : self.reviews.count
    }

    // Choose what cell will use
    func collectionView(_ collectionView: UICollectionView,
                        cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = movieCollectionView.dequeueReusableCell(
            withReuseIdentifier: HorizontalCollectionCell.identifier(), for: indexPath)
            as? HorizontalCollectionCell else {
                return UICollectionViewCell(frame: .zero)
        }

        let row = indexPath.row

        if !self.movies.isEmpty {
            let movie = self.movies[row]
            cell.setupUI(movie)
        } else {
            let review = self.reviews[row]
            cell.setupUI(review)
        }

        return cell
    }

    // styling each boxes for UX
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width: CGFloat = self.frame.width - 32
        return CGSize(width: width, height: self.height)
    }

    // margin cell
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout,
                        insetForSectionAt section: Int) -> UIEdgeInsets {
       return UIEdgeInsets(top: 0, left: 8, bottom: 0, right: 8)
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: true)
        viewAllTapped()
    }
}

extension HorizontalCollectionView {
    fileprivate func addView() {
        self.addSubview(containerView)
        [titleLabel, viewAllLabel, movieCollectionView]
            .forEach { containerView.addSubview($0) }

        containerView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }

        titleLabel.snp.makeConstraints { make in
            make.top.leading.equalTo(containerView).inset(8)
            make.trailing.lessThanOrEqualTo(viewAllLabel.snp.leading)
        }

        viewAllLabel.snp.makeConstraints { make in
            make.centerY.equalTo(titleLabel)
            make.trailing.equalTo(containerView).inset(8)
        }

        movieCollectionView.snp.makeConstraints { make in
            make.top.equalTo(titleLabel.snp.bottom).offset(16)
            make.leading.trailing.equalTo(containerView)
            make.bottom.equalTo(containerView).inset(16)
            make.height.equalTo(self.height)
        }
    }
}
