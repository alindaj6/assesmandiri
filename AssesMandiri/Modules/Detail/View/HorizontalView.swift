//
//  HorizontalView.swift
//  AssesMandiri
//
//  Created by Angga Setiawan on 09/02/24.
//

import UIKit

class HorizontalView: UIView {

    fileprivate var movie: MovieView?
    func setupUI(_ movie: MovieView) {
        self.movie = movie
        setImgView(path: movie.backdrop, placeholder: .unknown)

        imgView.layer.cornerRadius = 8
        imgView.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]

        setContent(title: movie.title,
                   release: movie.releaseDate?.toString(input: .yyyyMMdd, output: .ddMMMMyyyy) ?? "",
                   overview: movie.overview)
    }

    fileprivate var review: MovieReview?
    func setupUI(_ review: MovieReview, isVertical: Bool = false) {
        self.review = review

        imgView.isHidden = !isVertical

        if !isVertical {
            imgView.snp.removeConstraints()

            titleLabel.snp.remakeConstraints { make in
                make.top.leading.trailing.equalTo(containerView).inset(8)
            }

            overviewLabel.snp.remakeConstraints { make in
                make.top.equalTo(releaseDateLabel.snp.bottom).offset(16)
                make.bottom.lessThanOrEqualTo(containerView).inset(8)
                make.leading.trailing.equalTo(titleLabel)
            }
        } else {
            setImgView(path: review.authorDetails?.avatarPath ?? "", placeholder: .custom(image: UIImage(named: "user")!))

            imgView.contentMode = .scaleToFill
            imgView.layer.cornerRadius = 30
            imgView.snp.remakeConstraints { make in
                make.top.leading.equalTo(containerView).inset(8)
                make.size.equalTo(60)
            }

            titleLabel.snp.remakeConstraints { make in
                make.leading.equalTo(imgView.snp.trailing).offset(8)
                make.trailing.equalTo(containerView).offset(-8)
                make.centerY.equalTo(imgView)
            }

            releaseDateLabel.snp.remakeConstraints { make in
                make.top.equalTo(imgView.snp.bottom).offset(4)
                make.leading.equalTo(imgView)
                make.trailing.equalTo(titleLabel)
            }

            overviewLabel.snp.remakeConstraints { make in
                make.top.equalTo(releaseDateLabel.snp.bottom).offset(8)
                make.bottom.lessThanOrEqualTo(containerView).inset(8)
                make.leading.trailing.equalTo(releaseDateLabel)
            }

            overviewLabel.numberOfLines = 0
        }

        setContent(title: review.author,
                   release: review.updatedAt?.toString(input: .yyyyMMddTHHmmssZ, output: .ddMMMMyyyy) ?? "",
                   overview: review.content)
    }

    fileprivate func setImgView(path: String, placeholder: DefaultImage) {
        imgView.setOriginalImage(path: path, placeholder: placeholder)
    }

    fileprivate func setContent(title: String, release: String, overview: String) {
        titleLabel.text = title
        releaseDateLabel.text = release
        overviewLabel.text = overview
    }

    fileprivate lazy var containerView: UIView = {
        let view = UIView()
        view.backgroundColor = .white
        view.layer.applyCornerRadiusShadow(cornerRadiusValue: 8)
        view.isUserInteractionEnabled = true
        return view
    }()

    fileprivate lazy var imgView: UIImageView = {
        let imgView = UIImageView()
        imgView.contentMode = .scaleToFill
        imgView.image = UIImage()
        imgView.clipsToBounds = true
        return imgView
    }()

    fileprivate lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.text = ""
        label.font = .boldSystemFont(ofSize: 20)
        label.numberOfLines = 0
        return label
    }()

    fileprivate lazy var releaseDateLabel: UILabel = {
        let label = UILabel()
        label.text = ""
        label.numberOfLines = 3
        return label
    }()

    fileprivate lazy var overviewLabel: UILabel = {
        let label = UILabel()
        label.text = ""
        label.numberOfLines = 3
        return label
    }()

    override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = .clear
        self.addView()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension HorizontalView {
    fileprivate func addView() {
        self.addSubview(containerView)
        [imgView, titleLabel, releaseDateLabel, overviewLabel]
            .forEach { containerView.addSubview($0) }

        containerView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }

        imgView.snp.makeConstraints { make in
            make.top.leading.trailing.equalTo(containerView)
            make.height.equalTo(imgView.snp.width).dividedBy(1.777)
        }

        titleLabel.snp.makeConstraints { make in
            make.top.equalTo(imgView.snp.bottom).offset(12)
            make.leading.trailing.equalTo(imgView).inset(4)
        }

        releaseDateLabel.snp.makeConstraints { make in
            make.top.equalTo(titleLabel.snp.bottom).offset(4)
            make.leading.trailing.equalTo(titleLabel)
        }

        overviewLabel.snp.makeConstraints { make in
            make.top.equalTo(releaseDateLabel.snp.bottom).offset(16)
            make.bottom.lessThanOrEqualTo(containerView)
            make.leading.trailing.equalTo(titleLabel)
        }
    }
}
