//
//  LeftRightView.swift
//  AssesMandiri
//
//  Created by Angga Setiawan on 09/02/24.
//

import UIKit

class LeftRightView: UIView {

    func setupUI(leftTitle: String, leftContent: String, rightTitle: String, rightContent: String) {
        self.leftTitleLabel.text = leftTitle
        self.leftContentLabel.text = leftContent
        self.rightTitleLabel.text = rightTitle
        self.rightContentLabel.text = rightContent

        if rightTitle.isEmpty && rightContent.isEmpty {
            leftTitleLabel.snp.remakeConstraints { make in
                make.top.equalToSuperview()
                make.leading.trailing.equalToSuperview().inset(8)
            }

            leftContentLabel.snp.remakeConstraints { make in
                make.leading.trailing.equalTo(leftTitleLabel)
                make.top.equalTo(leftTitleLabel.snp.bottom).offset(4)
                make.bottom.equalToSuperview()
            }
            return
        }

        let leftTitleContent = leftTitle + leftContent
        let rightTitleContent = rightTitle + rightContent

        if rightTitleContent.count > leftTitleContent.count {
            leftContentLabel.snp.remakeConstraints { make in
                make.leading.trailing.equalTo(leftTitleLabel)
                make.top.equalTo(leftTitleLabel.snp.bottom).offset(4)
            }

            rightContentLabel.snp.remakeConstraints { make in
                make.top.equalTo(leftContentLabel)
                make.leading.trailing.equalTo(rightTitleLabel)
                make.bottom.equalToSuperview()
            }
        }
    }

    fileprivate var videos: [MovieVideo] = []
    func setupUITrailer(title: String, videos: [MovieVideo]) {
        self.videos = videos
        self.leftTitleLabel.text = title

        leftTitleLabel.snp.remakeConstraints { make in
            make.top.equalToSuperview()
            make.leading.trailing.equalToSuperview().inset(8)
        }

        leftContentLabel.snp.remakeConstraints { make in
            make.leading.trailing.equalTo(leftTitleLabel)
            make.top.equalTo(leftTitleLabel.snp.bottom).offset(4)
            make.bottom.equalToSuperview()
        }

        var targetStr = ""
        var textFocuses: [TextFocus] = []

        videos.indices.forEach { index in
            let text = index == videos.count - 1 ? "Trailer #\(index + 1)" : "Trailer #\(index + 1)\n"
            targetStr.append(text)
            let textFocus = TextFocus(text: text,
                                      color: .blue,
                                      font: self.leftContentLabel.font,
                                      underlineStyle: true)
            textFocuses.append(textFocus)
        }

        self.leftContentLabel.setText(targetStr, withFocuses: textFocuses)
        self.leftContentLabel.isUserInteractionEnabled = true
        self.leftContentLabel.addGestureRecognizer(
            UITapGestureRecognizer(target: self, action: #selector(openTrailer)))
    }

    var isLeftContentLink: Bool = false {
        didSet {
            let targetStr = self.leftContentLabel.text ?? ""
            let textFocus = TextFocus(text: targetStr,
                                      color: .blue,
                                      font: self.leftContentLabel.font,
                                      underlineStyle: true)
            self.leftContentLabel.setText(targetStr, withFocuses: [textFocus])
            self.leftContentLabel.isUserInteractionEnabled = true
            self.leftContentLabel.addGestureRecognizer(
                UITapGestureRecognizer(target: self, action: #selector(leftUrlTapped)))
        }
    }

    var isRightContentLink: Bool = false {
        didSet {
            let targetStr = self.rightContentLabel.text ?? ""
            let textFocus = TextFocus(text: targetStr,
                                      color: .blue,
                                      font: self.rightContentLabel.font,
                                      underlineStyle: true)
            self.rightContentLabel.setText(targetStr, withFocuses: [textFocus])
            self.rightContentLabel.isUserInteractionEnabled = true
            self.rightContentLabel.addGestureRecognizer(
                UITapGestureRecognizer(target: self, action: #selector(rightUrlTapped)))
        }
    }

    fileprivate lazy var leftTitleLabel: UILabel = {
        let label = UILabel()
        label.text = ""
        label.font = .boldSystemFont(ofSize: 16)
        label.textAlignment = .left
        label.numberOfLines = 0
        return label
    }()

    fileprivate lazy var leftContentLabel: UILabel = {
        let label = UILabel()
        label.text = ""
        label.font = .systemFont(ofSize: 14)
        label.textAlignment = .left
        label.numberOfLines = 0
        return label
    }()

    fileprivate lazy var rightTitleLabel: UILabel = {
        let label = UILabel()
        label.text = ""
        label.font = .boldSystemFont(ofSize: 16)
        label.textAlignment = .left
        label.numberOfLines = 0
        return label
    }()

    fileprivate lazy var rightContentLabel: UILabel = {
        let label = UILabel()
        label.text = ""
        label.font = .systemFont(ofSize: 14)
        label.textAlignment = .left
        label.numberOfLines = 0
        return label
    }()

    override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = .clear
        self.addView()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    @objc fileprivate func leftUrlTapped() {
        openURL(title: self.leftTitleLabel.text ?? "",
                urlString: self.leftContentLabel.text ?? "")
    }

    @objc fileprivate func rightUrlTapped() {
        openURL(title: self.rightTitleLabel.text ?? "",
                urlString: self.rightContentLabel.text ?? "")
    }

    fileprivate func openURL(title: String, urlString: String) {
        WebSourceFactory.startNavigation(
            with: .url(title: title,
                       origin: urlString, preferred: nil))
    }

    @objc fileprivate func openTrailer() {
        let alertController = UIAlertController(
            title: "Play Trailer?",
            message: "Please Choose",
            preferredStyle: .actionSheet)

        videos.enumerated().forEach { (index, video) in
            alertController.addAction(UIAlertAction(title: "Trailer #\(index + 1)\n", style: .default, handler: { (_) in
                let vc = YoutubeViewController.create(videoId: video.key)
                UINavigationController.topMostVc?.navigationController?.pushViewController(vc, animated: true)
            }))
        }

        alertController.addAction(UIAlertAction(title: "Cancel", style: .cancel))
        UINavigationController.topMostVc?.present(alertController, animated: true)
    }
}

extension LeftRightView {
    fileprivate func addView() {

        [leftTitleLabel, leftContentLabel,
         rightTitleLabel, rightContentLabel]
            .forEach { self.addSubview($0) }

        leftTitleLabel.snp.makeConstraints { make in
            make.top.equalToSuperview()
            make.leading.equalToSuperview().inset(8)
            make.trailing.equalTo(self.snp.centerX).inset(4)
        }

        leftContentLabel.snp.makeConstraints { make in
            make.leading.trailing.equalTo(leftTitleLabel)
            make.top.equalTo(leftTitleLabel.snp.bottom).offset(4)
            make.bottom.equalToSuperview()
        }

        rightTitleLabel.snp.makeConstraints { make in
            make.top.equalTo(leftTitleLabel)
            make.leading.equalTo(self.snp.centerX).offset(4)
            make.trailing.equalToSuperview().inset(8)
        }

        rightContentLabel.snp.makeConstraints { make in
            make.top.equalTo(leftContentLabel)
            make.leading.trailing.equalTo(rightTitleLabel)
        }
    }
}
