//
//  MovieDetailTitleView.swift
//  AssesMandiri
//
//  Created by Angga Setiawan on 09/02/24.
//

import UIKit

class MovieDetailTitleView: UIView {

    func setupUI(tagline: String, title: String, overview: String) {
        self.taglineLabel.text = tagline
        self.titleLabel.text = title
        self.overviewLabel.text = overview
    }

    fileprivate lazy var taglineLabel: UILabel = {
        let label = UILabel()
        label.text = ""
        label.font = .boldSystemFont(ofSize: 18)
        label.textAlignment = .left
        label.numberOfLines = 0
        return label
    }()

    fileprivate lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.text = ""
        label.font = .boldSystemFont(ofSize: 18)
        label.textAlignment = .left
        label.numberOfLines = 0
        return label
    }()

    fileprivate lazy var overviewLabel: UILabel = {
        let label = UILabel()
        label.text = ""
        label.font = .systemFont(ofSize: 14)
        label.textAlignment = .left
        label.numberOfLines = 0
        return label
    }()

    override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = .clear
        self.addView()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension MovieDetailTitleView {
    fileprivate func addView() {
        [taglineLabel, titleLabel, overviewLabel]
            .forEach { self.addSubview($0) }

        taglineLabel.snp.makeConstraints { make in
            make.top.equalToSuperview()
            make.leading.trailing.equalToSuperview().inset(8)
        }

        titleLabel.snp.makeConstraints { make in
            make.top.equalTo(taglineLabel.snp.bottom).offset(12)
            make.leading.trailing.equalTo(taglineLabel)
        }

        overviewLabel.snp.makeConstraints { make in
            make.top.equalTo(titleLabel.snp.bottom).offset(6)
            make.leading.trailing.equalTo(taglineLabel)
            make.bottom.equalToSuperview()
        }
    }
}
