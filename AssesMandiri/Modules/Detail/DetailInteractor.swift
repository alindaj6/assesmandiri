//
//  DetailInteractor.swift
//  AssesMandiri
//
//  Created by Angga Setiawan on 09/02/24.
//

import Foundation

protocol DetailInteractorInterface: AnyObject {
    func getMovieDetail(request: MovieDetailRequest) async -> MovieDetail?
    func getCollection(id: Int) async -> MovieCollectionDetail?
}

final class DetailInteractor: DetailInteractorInterface {
    func getMovieDetail(request: MovieDetailRequest) async -> MovieDetail? {
        do {
            let movieDetail = try await APIService.request(for: MovieDetail.self, route: MovieDBApi.getMovieDetail(params: request))
            return movieDetail
        } catch {
            print("ERROR: \(error.localizedDescription)")
            return nil
        }
    }

    func getCollection(id: Int) async -> MovieCollectionDetail? {
        do {
            let collection = try await APIService.request(for: MovieCollectionDetail.self, route: MovieDBApi.getCollection(id: id))
            return collection
        } catch {
            print("ERROR: \(error.localizedDescription)")
            return nil
        }
    }
}
