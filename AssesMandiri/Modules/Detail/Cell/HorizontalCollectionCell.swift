//
//  HorizontalCollectionCell.swift
//  AssesMandiri
//
//  Created by Angga Setiawan on 09/02/24.
//

import UIKit

class HorizontalCollectionCell: UICollectionViewCell {

    func setupUI(_ movie: MovieView) {
        self.horizontalView.setupUI(movie)
    }

    func setupUI(_ review: MovieReview) {
        self.horizontalView.setupUI(review)
    }

    fileprivate lazy var horizontalView: HorizontalView = {
        let view = HorizontalView()
        return view
    }()

    override init(frame: CGRect) {
        super.init(frame: frame)
        setupLayout()
        self.backgroundColor = .clear
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension HorizontalCollectionCell {
    fileprivate func setupLayout() {
        self.addSubview(horizontalView)
        horizontalView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
    }
}
