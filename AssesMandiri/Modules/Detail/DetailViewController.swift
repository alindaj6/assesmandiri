//
//  DetailViewController.swift
//  AssesMandiri
//
//  Created by Angga Setiawan on 09/02/24.
//

import UIKit
import ImageSlideshow

protocol DetailViewInterface: AnyObject {
    func setupView()
    func setTitle(with title: String)
    func showView()
}

class DetailViewController: UIViewController {

    fileprivate lazy var activityIndicator: UIActivityIndicatorView = {
        let indicator = UIActivityIndicatorView(style: .large)
        indicator.hidesWhenStopped = true
        indicator.isHidden = false
        indicator.startAnimating()
        return indicator
    }()

    fileprivate lazy var scrollView: UIScrollView = {
        let scrollView = UIScrollView()
        scrollView.showsVerticalScrollIndicator = false
        scrollView.isHidden = true
        return scrollView
    }()

    fileprivate lazy var imageSlideShow: ImageSlideshow = {
        let imgSlideShow = ImageSlideshow()
        imgSlideShow.activityIndicator = DefaultActivityIndicator(style: .medium, color: nil)
        imgSlideShow.slideshowInterval = 2
        imgSlideShow.circular = true
        imgSlideShow.contentScaleMode = .scaleAspectFit
        imgSlideShow.draggingEnabled = true
        return imgSlideShow
    }()

    fileprivate lazy var infoStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.distribution = .equalSpacing
        stackView.spacing = 16
        return stackView
    }()

    var presenter: DetailPresenterInterface = DetailPresenter(view: nil, router: nil, interactor: nil)

    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.notifyViewDidLoad()
    }
}

extension DetailViewController: DetailViewInterface {
    func setupView() {
        self.view.backgroundColor = .white
        self.addView()
    }

    func setTitle(with title: String) {
        self.title = title
    }

    // swiftlint:disable function_body_length
    func showView() {
        guard let movieDetail = presenter.movie else { return }

        if !infoStackView.arrangedSubviews.isEmpty {
            infoStackView.removeAllArrangedSubviews()
        }

        if !presenter.imageSources.isEmpty {
            imageSlideShow.setImageInputs(presenter.imageSources)
        }

        let movieDetailTitleView = MovieDetailTitleView()
        movieDetailTitleView.setupUI(tagline: movieDetail.tagline,
                                     title: movieDetail.title, overview: movieDetail.overview)

        let releaseRuntimeView = LeftRightView()
        releaseRuntimeView.setupUI(leftTitle: "Released",
                                   leftContent: movieDetail.releaseDate?.toString(input: .yyyyMMdd, output: .ddMMMMyyyy) ?? "",
                                   rightTitle: "Runtime",
                                   rightContent: "\(movieDetail.runtime) Minutes")

        let budgetRevenueView = LeftRightView()
        budgetRevenueView.setupUI(leftTitle: "Budget",
                                  leftContent: movieDetail.budget.toCurrency,
                                  rightTitle: "Revenue",
                                  rightContent: movieDetail.revenue.toCurrency)

        let homepageView = LeftRightView()
        homepageView.setupUI(leftTitle: "Homepage",
                             leftContent: movieDetail.homepage?.absoluteString ?? "",
                             rightTitle: "", rightContent: "")
        homepageView.isLeftContentLink = true

        let imdbView = LeftRightView()
        imdbView.setupUI(leftTitle: "IMDB",
                         leftContent: movieDetail.imdb?.absoluteString ?? "",
                         rightTitle: "", rightContent: "")
        imdbView.isLeftContentLink = true

        let popularityView = LeftRightView()
        popularityView.setupUI(leftTitle: "Popularity",
                               leftContent: "\(movieDetail.popularity)",
                               rightTitle: "Genres",
                               rightContent: movieDetail.genres.toString())

        let voteView = LeftRightView()
        voteView.setupUI(leftTitle: "Vote Average",
                         leftContent: "\(movieDetail.voteAverage)",
                         rightTitle: "Vote Count",
                         rightContent: "\(movieDetail.voteCount)")

        let trailerView = LeftRightView()
        trailerView.setupUITrailer(title: "Trailer", videos: presenter.videos)

        [movieDetailTitleView, releaseRuntimeView, budgetRevenueView,
         homepageView, imdbView, popularityView, voteView, trailerView]
            .forEach { infoStackView.addArrangedSubview($0) }

        let reviews = presenter.reviews
        if !reviews.isEmpty {
            let reviewCollectionView = HorizontalCollectionView()
            reviewCollectionView.setupUI(title: "Reviews", reviews: reviews, delegate: self)
            infoStackView.addArrangedSubview(reviewCollectionView)
        }

        let casts = presenter.casts
        if !casts.isEmpty {
            let view = HorizontalRoundedView()
            view.setupUI(title: "Casts", movieCredits: casts)
            infoStackView.addArrangedSubview(view)
        }

        let crews = presenter.crews
        if !crews.isEmpty {
            let view = HorizontalRoundedView()
            view.setupUI(title: "Crews", movieCredits: crews)
            infoStackView.addArrangedSubview(view)
        }

        if let movieCollection = presenter.collection {
            let movieCollectionView = HorizontalCollectionView()
            movieCollectionView.setupUI(title: "\(movieCollection.name)\n\(movieCollection.overview)", movies: movieCollection.parts)
            infoStackView.addArrangedSubview(movieCollectionView)
        }

        delay(interval: .milliseconds(500)) {
            self.activityIndicator.stopAnimating()
            self.activityIndicator.isHidden = true
            self.scrollView.isHidden = false
        }
    }
}

extension DetailViewController: HorizontalCollectionDelegate {
    func didViewAllTapped() {
        if let movieId = presenter.movie?.id {
            presenter.goToReview(movieId: movieId)
        }
    }
}

extension DetailViewController {
    fileprivate func addView() {
        self.view.addSubview(activityIndicator)
        self.view.addSubview(scrollView)
        [imageSlideShow, infoStackView]
            .forEach { scrollView.addSubview($0) }

        activityIndicator.snp.makeConstraints { make in
            make.center.equalToSuperview()
        }

        scrollView.snp.makeConstraints { make in
            make.top.equalTo(self.view.safeAreaLayoutGuide.snp.top)
            make.bottom.equalToSuperview()
            make.leading.trailing.width.equalToSuperview()
        }

        imageSlideShow.snp.makeConstraints { make in
            make.top.equalTo(scrollView)
            make.leading.trailing.equalTo(scrollView)
            make.height.equalTo(imageSlideShow.snp.width).dividedBy(1.778)
        }

        infoStackView.snp.makeConstraints { make in
            make.top.equalTo(imageSlideShow.snp.bottom).offset(16)
            make.leading.trailing.width.equalTo(scrollView)
            make.bottom.equalTo(scrollView).inset(16)
        }
    }
}
