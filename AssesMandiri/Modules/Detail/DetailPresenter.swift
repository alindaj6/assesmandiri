//
//  DetailPresenter.swift
//  AssesMandiri
//
//  Created by Angga Setiawan on 09/02/24.
//

import Foundation
import ImageSlideshow

protocol DetailPresenterInterface: AnyObject {
    func notifyViewDidLoad()
    var movieId: Int? { get set }
    func fetchDatas()
    var movie: MovieDetail? { get }
    var imageSources: [InputSource] { get }
    var collection: MovieCollectionDetail? { get }
    var casts: [MovieCredit] { get }
    var crews: [MovieCredit] { get }
    var reviews: [MovieReview] { get }
    var videos: [MovieVideo] { get }
    func goToReview(movieId: Int)
}

final class DetailPresenter {

    var movieId: Int?

    private weak var view: DetailViewInterface?
    private var router: DetailRouterInterface?
    private var interactor: DetailInteractorInterface?

    private var movieDetail: MovieDetail?
    private var movieCollection: MovieCollectionDetail?

    init(view: DetailViewInterface?, router: DetailRouterInterface?, interactor: DetailInteractorInterface?) {
        self.view = view
        self.router = router
        self.interactor = interactor
    }
}

// MARK: - Interface Setup
extension DetailPresenter: DetailPresenterInterface {
    func notifyViewDidLoad() {
        view?.setupView()
        view?.setTitle(with: "Movie")
        fetchDatas()
    }

    func fetchDatas() {
        Task { @MainActor in
            guard let movieId = self.movieId else { return }

            let movieDetailRequest = MovieDetailRequest(movieId: movieId, contents: [.images, .videos, .reviews, .credits])
            movieDetail = await interactor?.getMovieDetail(request: movieDetailRequest)

            if let idCollection = movieDetail?.collection?.id {
                movieCollection = await interactor?.getCollection(id: idCollection)
            }

            view?.showView()
        }
    }

    var movie: MovieDetail? {
        return movieDetail
    }

    var imageSources: [InputSource] {
        if let backdrops = movie?.images?.backdrops {
            var tempSources: [InputSource] = []
            for backdrop in backdrops {
                let imgURL = "https://image.tmdb.org/t/p/original/\(backdrop.filePath)"
                if let source = KingfisherSource(urlString: imgURL, placeholder: UIImage()) {
                    tempSources.append(source)
                }
            }
            return tempSources
        }
        return []
    }

    var collection: MovieCollectionDetail? {
        return movieCollection
    }

    var casts: [MovieCredit] {
        return movieDetail?.credits?.casts ?? []
    }

    var crews: [MovieCredit] {
        return movieDetail?.credits?.crews ?? []
    }

    var reviews: [MovieReview] {
        return movieDetail?.reviews ?? []
    }

    var videos: [MovieVideo] {
        return movieDetail?.videos.filter { $0.site == .youtube && $0.type == .trailer } ?? []
    }

    func goToReview(movieId: Int) {
        router?.navigateToReview(from: view!, movieId: movieId)
    }
}
