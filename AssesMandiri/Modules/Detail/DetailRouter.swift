//
//  DetailRouter.swift
//  AssesMandiri
//
//  Created by Angga Setiawan on 09/02/24.
//

import Foundation

import UIKit

protocol DetailRouterInterface: AnyObject {
    func navigateToReview(from view: DetailViewInterface, movieId: Int)
}

final class DetailRouter: DetailRouterInterface {

    static func createModule(with movieId: Int) -> UIViewController {
        let router = DetailRouter()
        let viewController = DetailViewController()
        let interactor = DetailInteractor()
        let presenter = DetailPresenter(view: viewController, router: router, interactor: interactor)
        presenter.movieId = movieId

        viewController.presenter = presenter
        return viewController
    }

    func navigateToReview(from view: DetailViewInterface, movieId: Int) {
        let destination = ReviewRouter.createModule(with: movieId)
        if let sourceView = view as? UIViewController {
            sourceView.navigationController?.pushViewController(destination, animated: true)
        }
    }
}
