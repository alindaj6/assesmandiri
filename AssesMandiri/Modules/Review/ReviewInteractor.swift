//
//  ReviewInteractor.swift
//  AssesMandiri
//
//  Created by Angga Setiawan on 10/02/24.
//

import Foundation

protocol ReviewInteractorInterface: AnyObject {
    func getReviews(params: MovieReviewRequest) async -> AppPagedResponse<MovieReview>?
}

final class ReviewInteractor: ReviewInteractorInterface {
    func getReviews(params: MovieReviewRequest) async -> AppPagedResponse<MovieReview>? {
        do {
            let reviews = try await APIService.request(for: AppPagedResponse<MovieReview>.self, route: MovieDBApi.getReviews(params: params))
            return reviews
        } catch {
            print("ERROR: \(error.localizedDescription)")
            return nil
        }
    }
}
