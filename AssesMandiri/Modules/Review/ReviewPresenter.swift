//
//  ReviewPresenter.swift
//  AssesMandiri
//
//  Created by Angga Setiawan on 10/02/24.
//

import Foundation

protocol ReviewPresenterInterface: AnyObject {
    func notifyViewDidLoad()
    var movieId: Int? { get set }
    func fetchDatas()
    func getReviewBy(_ row: Int) -> MovieReview?
    var getItemCount: Int { get }
}

final class ReviewPresenter {

    var movieId: Int?

    private weak var view: ReviewViewInterface?
    private var router: ReviewRouterInterface?
    private var interactor: ReviewInteractorInterface?

    private var currentPage: Int = 1
    private var nextPage: Int = 1
    private var hasNextPage: Bool = true
    private var reviews: [MovieReview] = []

    init(view: ReviewViewInterface?, router: ReviewRouterInterface?, interactor: ReviewInteractorInterface?) {
        self.view = view
        self.router = router
        self.interactor = interactor
    }
}

// MARK: - Interface Setup
extension ReviewPresenter: ReviewPresenterInterface {
    func notifyViewDidLoad() {
        view?.setupView()
        view?.setTitle(with: "Reviews")
        fetchDatas()
    }

    func fetchDatas() {
        Task { @MainActor in
            guard hasNextPage else { return }
            guard let movieId = self.movieId else { return }

            let movieReviewRequest = MovieReviewRequest(page: nextPage, movieId: movieId)
            let response = await interactor?.getReviews(params: movieReviewRequest)

            currentPage = response?.currentPage ?? 1
            nextPage = response?.nextPage ?? 1
            hasNextPage = response?.hasNextPage ?? false

            if let reviews = response?.data, !reviews.isEmpty {
                self.reviews.append(contentsOf: reviews)
            }

            view?.showView()
        }
    }

    func getReviewBy(_ row: Int) -> MovieReview? {
        if reviews.isEmpty { return nil }
        return reviews[row]
    }

    var getItemCount: Int {
        return reviews.count
    }
}
