//
//  ReviewRouter.swift
//  AssesMandiri
//
//  Created by Angga Setiawan on 10/02/24.
//

import UIKit

protocol ReviewRouterInterface: AnyObject { }

final class ReviewRouter: ReviewRouterInterface {

    static func createModule(with movieId: Int) -> UIViewController {
        let router = ReviewRouter()
        let viewController = ReviewViewController()
        let interactor = ReviewInteractor()
        let presenter = ReviewPresenter(view: viewController, router: router, interactor: interactor)
        presenter.movieId = movieId

        viewController.presenter = presenter
        return viewController
    }
}
