//
//  ReviewViewController.swift
//  AssesMandiri
//
//  Created by Angga Setiawan on 10/02/24.
//

import UIKit

protocol ReviewViewInterface: AnyObject {
    func setupView()
    func setTitle(with title: String)
    func showView()
}

class ReviewViewController: UIViewController {

    fileprivate lazy var reviewTableView: UITableView = {
        let tableView = UITableView()
        tableView.allowsSelection = true
        tableView.dataSource = self
        tableView.estimatedRowHeight = 1000
        tableView.register(ReviewTableCell.self, forCellReuseIdentifier: ReviewTableCell.identifier())
        tableView.rowHeight = UITableView.automaticDimension
        tableView.showsVerticalScrollIndicator = false
        tableView.separatorColor = .clear
        tableView.backgroundColor = .clear
        tableView.isHidden = false
        return tableView
    }()

    var presenter: ReviewPresenterInterface = ReviewPresenter(view: nil, router: nil, interactor: nil)

    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.notifyViewDidLoad()
    }
}

// MARK: - Interface Setup
extension ReviewViewController: ReviewViewInterface {

    func setupView() {
        self.view.backgroundColor = .white
        self.addView()
    }

    func setTitle(with title: String) {
        self.title = title
    }

    func showView() {
        self.reviewTableView.reloadData()
    }
}

extension ReviewViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter.getItemCount
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = reviewTableView.dequeueReusableCell(withIdentifier: ReviewTableCell.identifier(), for: indexPath) as? ReviewTableCell else { return UITableViewCell() }

        let row = indexPath.row

        if let review = presenter.getReviewBy(row) {
            cell.setupUI(review: review)
        }

        if row == presenter.getItemCount - 3 {
            presenter.fetchDatas()
        }

        return cell
    }
}

extension ReviewViewController {
    fileprivate func addView() {
        self.view.addSubview(reviewTableView)
        reviewTableView.snp.makeConstraints { make in
            make.top.equalTo(self.view.safeAreaLayoutGuide.snp.top)
            make.bottom.equalTo(self.view.safeAreaLayoutGuide.snp.bottom)
            make.leading.trailing.equalToSuperview()
        }
    }
}
