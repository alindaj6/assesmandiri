//
//  ReviewTableCell.swift
//  AssesMandiri
//
//  Created by Angga Setiawan on 10/02/24.
//

import UIKit

class ReviewTableCell: UITableViewCell {

    func setupUI(review: MovieReview) {
        horizontalView.setupUI(review, isVertical: true)
    }

    fileprivate lazy var horizontalView: HorizontalView = {
        let view = HorizontalView()
        return view
    }()

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupLayout()
        self.backgroundColor = UIColor.clear
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension ReviewTableCell {
    fileprivate func setupLayout() {
        self.addSubview(horizontalView)
        horizontalView.snp.makeConstraints { make in
            make.edges.equalToSuperview().inset(8)
        }
    }
}
