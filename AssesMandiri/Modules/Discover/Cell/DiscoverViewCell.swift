//
//  DiscoverViewCell.swift
//  AssesMandiri
//
//  Created by Angga Setiawan on 09/02/24.
//

import UIKit

class DiscoverViewCell: UITableViewCell {

    func setupUI(movie: MovieView) {
        movieDiscoverView.setupUI(movie: movie)
    }

    fileprivate lazy var movieDiscoverView: MovieDiscoverView = {
        let view = MovieDiscoverView()
        return view
    }()

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupLayout()
        self.backgroundColor = UIColor.clear
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension DiscoverViewCell {
    fileprivate func setupLayout() {
        self.addSubview(movieDiscoverView)
        movieDiscoverView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
    }
}
