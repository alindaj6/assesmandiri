//
//  MovieDiscoverView.swift
//  AssesMandiri
//
//  Created by Angga Setiawan on 09/02/24.
//

import UIKit

class MovieDiscoverView: UIView {

    func setupUI(movie: MovieView) {
        self.titleLabel.text = movie.title
        self.subtitleLabel.text = movie.overview

        poster.setOriginalImage(path: movie.poster, placeholder: .unknown)
    }

    fileprivate lazy var containerView: UIView = {
        let view = UIView()
        return view
    }()

    fileprivate lazy var poster: UIImageView = {
        let imgView = UIImageView()
        imgView.contentMode = .scaleAspectFit
        imgView.image = UIImage()
        imgView.layer.cornerRadius = 8
        imgView.clipsToBounds = true
        return imgView
    }()

    fileprivate lazy var titleLabel: UILabel = {
        let label = UILabel()
        label.font = .boldSystemFont(ofSize: 20)
        label.numberOfLines = 0
        label.lineBreakMode = .byTruncatingTail
        return label
    }()

    fileprivate lazy var subtitleLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 7
        return label
    }()

    override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = .white
        self.addView()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension MovieDiscoverView {
    fileprivate func addView() {
        self.addSubview(containerView)
        [poster, titleLabel, subtitleLabel].forEach {
            containerView.addSubview($0)
        }

        containerView.snp.makeConstraints { make in
            make.edges.equalToSuperview().inset(8)
        }

        poster.snp.makeConstraints { make in
            make.top.bottom.equalTo(containerView)
            make.leading.equalTo(containerView).inset(16)
            make.trailing.equalTo(containerView.snp.centerX)
            make.height.equalTo(poster.snp.width).dividedBy(0.667)
        }

        titleLabel.snp.makeConstraints { make in
            make.top.equalTo(containerView)
            make.leading.equalTo(containerView.snp.centerX).offset(8)
            make.trailing.equalTo(containerView).inset(16)
        }

        subtitleLabel.snp.makeConstraints { make in
            make.top.equalTo(titleLabel.snp.bottom).offset(8)
            make.bottom.lessThanOrEqualTo(poster)
            make.leading.trailing.equalTo(titleLabel)
        }
    }
}
