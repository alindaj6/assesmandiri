//
//  DiscoverRouter.swift
//  AssesMandiri
//
//  Created by Angga Setiawan on 09/02/24.
//

import UIKit

protocol DiscoverRouterInterface: AnyObject {
    func navigateToDetail(from view: DiscoverViewInterface, movieId: Int)
}

final class DiscoverRouter: DiscoverRouterInterface {

    static func createModule(with genre: MovieGenre) -> UIViewController {
        let router = DiscoverRouter()
        let viewController = DiscoverViewController()
        let interactor = DiscoverInteractor()
        let presenter = DiscoverPresenter(view: viewController, router: router, interactor: interactor)
        presenter.genre = genre

        viewController.presenter = presenter
        return viewController
    }

    func navigateToDetail(from view: DiscoverViewInterface, movieId: Int) {
        let destination = DetailRouter.createModule(with: movieId)
        if let sourceView = view as? UIViewController {
            sourceView.navigationController?.pushViewController(destination, animated: true)
        }
    }
}
