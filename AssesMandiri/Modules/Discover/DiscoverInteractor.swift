//
//  DiscoverInteractor.swift
//  AssesMandiri
//
//  Created by Angga Setiawan on 09/02/24.
//

import Foundation

protocol DiscoverInteractorInterface: AnyObject {
    func getDiscovers(param: MovieDiscoverRequest) async -> AppPagedResponse<Movie>?
}

final class DiscoverInteractor: DiscoverInteractorInterface {

    func getDiscovers(param: MovieDiscoverRequest) async -> AppPagedResponse<Movie>? {
        do {
            let movies = try await APIService.request(for: AppPagedResponse<Movie>.self, route: MovieDBApi.getDiscoverMovie(param: param))
            return movies
        } catch {
            print("ERROR: \(error.localizedDescription)")
            return nil
        }
    }
}
