//
//  DiscoverViewController.swift
//  AssesMandiri
//
//  Created by Angga Setiawan on 09/02/24.
//

import UIKit

protocol DiscoverViewInterface: AnyObject {
    func setupView()
    func setTitle(with title: String)
    func showView()
}

class DiscoverViewController: UIViewController {

    fileprivate lazy var movieTableView: UITableView = {
        let tableView = UITableView()
        tableView.allowsSelection = true
        tableView.dataSource = self
        tableView.delegate = self
        tableView.estimatedRowHeight = 1000
        tableView.register(DiscoverViewCell.self, forCellReuseIdentifier: DiscoverViewCell.identifier())
        tableView.rowHeight = UITableView.automaticDimension
        tableView.showsVerticalScrollIndicator = false
        tableView.separatorColor = .clear
        tableView.backgroundColor = .clear
        tableView.isHidden = false
        return tableView
    }()

    var presenter: DiscoverPresenterInterface = DiscoverPresenter(view: nil, router: nil, interactor: nil)

    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.notifyViewDidLoad()
    }
}

// MARK: - Interface Setup
extension DiscoverViewController: DiscoverViewInterface {

    func setupView() {
        self.view.backgroundColor = .white
        self.addView()
    }

    func setTitle(with title: String) {
        self.title = title
    }

    func showView() {
        self.movieTableView.reloadData()
    }
}

extension DiscoverViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter.getItemCount
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = movieTableView.dequeueReusableCell(withIdentifier: DiscoverViewCell.identifier(), for: indexPath) as? DiscoverViewCell else { return UITableViewCell() }

        let row = indexPath.row

        if let movie = presenter.getMovieBy(row) {
            cell.setupUI(movie: movie)
        }

        if row == presenter.getItemCount - 3 {
            presenter.fetchDatas()
        }

        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let row = indexPath.row
        if let movieId = presenter.getMovieBy(row)?.id {
            presenter.goToDetail(movieId: movieId)
        }
    }
}

extension DiscoverViewController {
    fileprivate func addView() {
        self.view.addSubview(movieTableView)
        movieTableView.snp.makeConstraints { make in
            make.top.equalTo(self.view.safeAreaLayoutGuide.snp.top)
            make.bottom.equalTo(self.view.safeAreaLayoutGuide.snp.bottom)
            make.leading.trailing.equalToSuperview()
        }
    }
}
