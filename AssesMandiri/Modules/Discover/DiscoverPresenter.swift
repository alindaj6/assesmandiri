//
//  DiscoverPresenter.swift
//  AssesMandiri
//
//  Created by Angga Setiawan on 09/02/24.
//

import Foundation

protocol DiscoverPresenterInterface: AnyObject {
    func notifyViewDidLoad()
    var genre: MovieGenre? { get set }
    func fetchDatas()
    func getMovieBy(_ row: Int) -> Movie?
    var getItemCount: Int { get }
    func goToDetail(movieId: Int)
}

final class DiscoverPresenter {

    var genre: MovieGenre?

    private weak var view: DiscoverViewInterface?
    private var router: DiscoverRouterInterface?
    private var interactor: DiscoverInteractorInterface?

    private var currentPage: Int = 1
    private var nextPage: Int = 1
    private var hasNextPage: Bool = true
    private var movies: [Movie] = []

    init(view: DiscoverViewInterface?, router: DiscoverRouterInterface?, interactor: DiscoverInteractorInterface?) {
        self.view = view
        self.router = router
        self.interactor = interactor
    }
}

// MARK: - Interface Setup
extension DiscoverPresenter: DiscoverPresenterInterface {

    func notifyViewDidLoad() {
        view?.setupView()
        view?.setTitle(with: "Discover")
        fetchDatas()
    }

    func fetchDatas() {
        Task { @MainActor in
            guard hasNextPage else { return }
            guard let genreId = genre?.id else { return }

            let request = MovieDiscoverRequest(page: nextPage, genreId: genreId)
            let response = await interactor?.getDiscovers(param: request)

            currentPage = response?.currentPage ?? 1
            nextPage = response?.nextPage ?? 1
            hasNextPage = response?.hasNextPage ?? false

            if let movies = response?.data, !movies.isEmpty {
                self.movies.append(contentsOf: movies)
            }

            view?.showView()
        }
    }

    func getMovieBy(_ row: Int) -> Movie? {
        if movies.isEmpty { return nil }
        return movies[row]
    }

    var getItemCount: Int {
        movies.count
    }

    func goToDetail(movieId: Int) {
        router?.navigateToDetail(from: view!, movieId: movieId)
    }
}
