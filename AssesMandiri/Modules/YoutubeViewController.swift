//
//  YoutubeViewController.swift
//  AssesMandiri
//
//  Created by Angga Setiawan on 10/02/24.
//

import UIKit
import YoutubeKit

class YoutubeViewController: UIViewController {

    internal lazy var videoView: YTSwiftyPlayer = {
        let view = YTSwiftyPlayer()
        return view
    }()

    private var player: YTSwiftyPlayer!
    private var videoId: String = ""

    static func create(videoId: String) -> YoutubeViewController {
        let vc = YoutubeViewController()
        vc.videoId = videoId
        return vc
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Trailer"

        // Create a new player
        player = YTSwiftyPlayer(
            frame: .zero,
            playerVars: [
                .playsInline(false),
                .videoID(videoId),
                .loopVideo(true),
                .showRelatedVideo(false),
                .autoplay(true)
            ])

        view = player
//        player.delegate = self
        player.loadDefaultPlayer()
    }
}
