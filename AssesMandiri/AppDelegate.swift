//
//  AppDelegate.swift
//  AssesMandiri
//
//  Created by Angga Setiawan on 09/02/24.
//

import UIKit
import SnapKit
import Wormholy

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication,
                     didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {

        let homeController = HomeRouter.createModule()
        window?.rootViewController = homeController
        window?.makeKeyAndVisible()

        Wormholy.shakeEnabled = true

        // Hide Autolayout Warning
        UserDefaults.standard.set(false, forKey: "_UIConstraintBasedLayoutLogUnsatisfiable")

        return true
    }
}
