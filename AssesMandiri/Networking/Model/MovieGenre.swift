//
//  MovieGenre.swift
//  AssesMandiri
//
//  Created by Angga Setiawan on 09/02/24.
//

import Foundation

public extension [MovieGenre] {
    func toString(separator: String = ", ") -> String {
        return self.map { $0.name }.joined(separator: separator)
    }
}

public struct Genres {
    public let genres: [MovieGenre]

    public enum CodingKeys: String, CodingKey {
        case genres
    }
}

extension Genres: Decodable {
    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        genres = try container.decodeIfPresent([MovieGenre].self, forKey: .genres) ?? []
    }
}

public struct MovieGenre {
    public let id: Int
    public let name: String

    private enum CodingKeys: String, CodingKey {
        case id, name
    }
}

extension MovieGenre: Decodable {
    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        id = try container.decodeIfPresent(Int.self, forKey: .id) ?? 0
        name = try container.decodeIfPresent(String.self, forKey: .name) ?? ""
    }
}
