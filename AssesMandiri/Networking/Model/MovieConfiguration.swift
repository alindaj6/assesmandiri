//
//  MovieConfiguration.swift
//  AssesMandiri
//
//  Created by Angga Setiawan on 09/02/24.
//

import Foundation

public enum MovieDBSizes: String {
    case w300
    case w780
    case w1280

    case w45
    case w92
    case w154
    case w185
    case w500

    case w342

    case h632

    case original

    case unknown
}

public struct MovieDBImagesConfiguration: Decodable {
    public let movieDBConfiguration: MovieDBConfiguration?

    private enum CodingKeys: String, CodingKey {
        case images
    }

    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.movieDBConfiguration = try container.decodeIfPresent(MovieDBConfiguration.self, forKey: .images)
    }
}

public struct MovieDBConfiguration {
    public let baseURL: URL?
    public let secureBaseURL: URL?
    public let backdropSizes: [MovieDBSizes]
    public let logoSizes: [MovieDBSizes]
    public let posterSizes: [MovieDBSizes]
    public let profileSizes: [MovieDBSizes]
    public let stillSizes: [MovieDBSizes]

    private enum CodingKeys: String, CodingKey {
        case base_url, secure_base_url
        case backdrop_sizes, logo_sizes
        case poster_sizes, profile_sizes
        case still_sizes
    }
}

extension MovieDBConfiguration: Decodable {
    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        baseURL = URL(string: try container.decodeIfPresent(String.self, forKey: .base_url) ?? "")
        secureBaseURL = URL(string: try container.decodeIfPresent(String.self, forKey: .secure_base_url) ?? "")

        let backdropSizess = try container.decodeIfPresent([String].self, forKey: .backdrop_sizes) ?? []
        var movieBackdropSizes: [MovieDBSizes] = []
        for size in backdropSizess {
            if let size = MovieDBSizes(rawValue: size) {
                movieBackdropSizes.append(size)
            }
        }
        backdropSizes = movieBackdropSizes

        let logoSizess = try container.decodeIfPresent([String].self, forKey: .logo_sizes) ?? []
        var movieLogoSizes: [MovieDBSizes] = []
        for size in logoSizess {
            if let size = MovieDBSizes(rawValue: size) {
                movieLogoSizes.append(size)
            }
        }
        logoSizes = movieLogoSizes

        let posterSizess = try container.decodeIfPresent([String].self, forKey: .poster_sizes) ?? []
        var moviePosterSizes: [MovieDBSizes] = []
        for size in posterSizess {
            if let size = MovieDBSizes(rawValue: size) {
                moviePosterSizes.append(size)
            }
        }
        posterSizes = moviePosterSizes

        let profileSizess = try container.decodeIfPresent([String].self, forKey: .profile_sizes) ?? []
        var movieProfileSizes: [MovieDBSizes] = []
        for size in profileSizess {
            if let size = MovieDBSizes(rawValue: size) {
                movieProfileSizes.append(size)
            }
        }
        profileSizes = movieProfileSizes

        let stillSizess = try container.decodeIfPresent([String].self, forKey: .still_sizes) ?? []
        var movieStillSizes: [MovieDBSizes] = []
        for size in stillSizess {
            if let size = MovieDBSizes(rawValue: size) {
                movieStillSizes.append(size)
            }
        }
        stillSizes = movieStillSizes
    }
}
