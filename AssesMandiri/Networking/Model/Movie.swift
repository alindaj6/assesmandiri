//
//  Movie.swift
//  AssesMandiri
//
//  Created by Angga Setiawan on 09/02/24.
//

import Foundation

public protocol MovieView {
    var id: Int { get }
    var title: String { get }
    var overview: String { get }
    var backdrop: String { get }
    var releaseDate: Date? { get }
    var poster: String { get }
}

public struct Movie {
    public let id: Int
    public let title: String
    public let overview: String
    public let poster: String
    public let backdrop: String
    public let releaseDate: Date?

    private enum CodingKeys: String, CodingKey {
        case id, title, overview, poster_path
        case backdropPath = "backdrop_path"
        case releaseDate = "release_date"
    }
}

extension Movie: Decodable {
    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        id = try container.decodeIfPresent(Int.self, forKey: .id) ?? 0
        title = try container.decodeIfPresent(String.self, forKey: .title) ?? ""
        overview = try container.decodeIfPresent(String.self, forKey: .overview) ?? ""
        poster = try container.decodeIfPresent(String.self, forKey: .poster_path) ?? ""
        backdrop = try container.decodeIfPresent(String.self, forKey: .backdropPath) ?? ""

        let releaseDatee = try container.decodeIfPresent(String.self, forKey: .releaseDate) ?? ""
        releaseDate = releaseDatee.toDate(input: .yyyyMMdd)
    }
}

extension Movie: MovieView {}
