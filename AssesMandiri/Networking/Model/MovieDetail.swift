//
//  MovieDetail.swift
//  AssesMandiri
//
//  Created by Angga Setiawan on 09/02/24.
//

import Foundation

public enum MovieVideoSite: String {
    case youtube = "YouTube"

    case unknown
}

public enum MovieVideoType: String {
    case behindTheScenes = "Behind the Scenes"
    case featurette = "Featurette"
    case clip = "Clip"
    case teaser = "Teaser"
    case trailer = "Trailer"

    case unknown
}

public struct MovieDetail {

    public let id: Int

    public let tagline: String
    public let title: String
    public let overview: String

    public let releaseDate: Date?
    public let runtime: Int

    public let budget: Int
    public let revenue: Int

    public let homepage: URL?

    public let imdb: URL?

    public let popularity: Double

    public let voteAverage: Double
    public let voteCount: Int

    public let collection: MovieCollection?
    public let genres: [MovieGenre]
    public let productionCompanies: [MovieProductionCompanies]
    public let productionCountries: [MovieProductionCountries]
    public let spokenLanguages: [MovieSpokenLanguages]

    public let images: MovieImages?

    public let credits: MovieCredits?

    public let reviews: [MovieReview]

    public let videos: [MovieVideo]

    private enum CodingKeys: String, CodingKey {
        case id, title, overview, poster_path
        case backdropPath = "backdrop_path"
        case releaseDate = "release_date"

        case budget, homepage
        case imdbId = "imdb_id"
        case popularity, revenue, runtime
        case tagline
        case voteAverage = "vote_average"
        case voteCount = "vote_count"

        case collection = "belongs_to_collection"
        case genres
        case productionCompanies = "production_companies"
        case productionCountries = "production_countries"
        case spokenLanguages = "spoken_languages"

        case videos, images, credits, reviews
    }

    private enum VideosKeys: String, CodingKey {
        case results
    }

    private enum ImagesKeys: String, CodingKey {
        case backdrops, posters, logos
    }
}

extension MovieDetail: Decodable {
    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        id = try container.decodeIfPresent(Int.self, forKey: .id) ?? 0
        title = try container.decodeIfPresent(String.self, forKey: .title) ?? ""
        overview = try container.decodeIfPresent(String.self, forKey: .overview) ?? ""

        let releaseDatee = try container.decodeIfPresent(String.self, forKey: .releaseDate) ?? ""
        releaseDate = releaseDatee.toDate(input: .yyyyMMdd)

        budget = try container.decodeIfPresent(Int.self, forKey: .budget) ?? 0
        homepage = URL(string: try container.decodeIfPresent(String.self, forKey: .homepage) ?? "")

        let imdbId = try container.decodeIfPresent(String.self, forKey: .imdbId) ?? ""
        imdb = URL(string: "https://www.imdb.com/title/\(imdbId)")

        popularity = try container.decodeIfPresent(Double.self, forKey: .popularity) ?? 0.0
        revenue = try container.decodeIfPresent(Int.self, forKey: .revenue) ?? 0
        runtime = try container.decodeIfPresent(Int.self, forKey: .runtime) ?? 0
        tagline = try container.decodeIfPresent(String.self, forKey: .tagline) ?? ""
        voteAverage = try container.decodeIfPresent(Double.self, forKey: .voteAverage) ?? 0.0
        voteCount = try container.decodeIfPresent(Int.self, forKey: .voteCount) ?? 0

        collection = try container.decodeIfPresent(MovieCollection.self, forKey: .collection)
        genres = try container.decodeIfPresent([MovieGenre].self, forKey: .genres) ?? []
        productionCompanies = try container.decodeIfPresent([MovieProductionCompanies].self, forKey: .productionCompanies) ?? []
        productionCountries = try container.decodeIfPresent([MovieProductionCountries].self, forKey: .productionCountries) ?? []
        spokenLanguages = try container.decodeIfPresent([MovieSpokenLanguages].self, forKey: .spokenLanguages) ?? []

        images = try container.decodeIfPresent(MovieImages.self, forKey: .images)
        credits = try container.decodeIfPresent(MovieCredits.self, forKey: .credits)

        let reviewss = try container.decodeIfPresent(AppPagedResponse<MovieReview>.self, forKey: .reviews)
        reviews = reviewss?.data ?? []

        let videoss = try container.decodeIfPresent(AppPagedResponse<MovieVideo>.self, forKey: .videos)
        videos = videoss?.data ?? []
    }
}

public struct MovieCollection {
    public let id: Int
    public let name: String
    public let poster: String
    public let backdrop: String

    private enum CodingKeys: String, CodingKey {
        case id, name
        case poster = "poster_path"
        case backdrop = "backdrop_path"
    }
}

extension MovieCollection: Decodable {
    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        id = try container.decodeIfPresent(Int.self, forKey: .id) ?? 0
        name = try container.decodeIfPresent(String.self, forKey: .name) ?? ""
        poster = try container.decodeIfPresent(String.self, forKey: .poster) ?? ""
        backdrop = try container.decodeIfPresent(String.self, forKey: .backdrop) ?? ""
    }
}

public struct MovieProductionCompanies {
    public let id: Int
    public let name: String
    public let logo: String
    public let originCountry: String

    private enum CodingKeys: String, CodingKey {
        case id, name
        case logoPath = "logo_path"
        case originCountry = "origini_country"
    }
}

extension MovieProductionCompanies: Decodable {
    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        id = try container.decodeIfPresent(Int.self, forKey: .id) ?? 0
        name = try container.decodeIfPresent(String.self, forKey: .name) ?? ""
        logo = try container.decodeIfPresent(String.self, forKey: .logoPath) ?? ""
        originCountry = try container.decodeIfPresent(String.self, forKey: .originCountry) ?? ""
    }
}

public struct MovieProductionCountries {
    public let iso_3166_1: String
    public let name: String

    private enum CodingKeys: String, CodingKey {
        case iso_3166_1, name
    }
}

extension MovieProductionCountries: Decodable {
    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        iso_3166_1 = try container.decodeIfPresent(String.self, forKey: .iso_3166_1) ?? ""
        name = try container.decodeIfPresent(String.self, forKey: .name) ?? ""
    }
}

public struct MovieSpokenLanguages {
    public let iso_639_1: String
    public let name: String
    public let englishName: String

    private enum CodingKeys: String, CodingKey {
        case iso_639_1, name
        case englishName = "english_name"
    }
}

extension MovieSpokenLanguages: Decodable {
    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        iso_639_1 = try container.decodeIfPresent(String.self, forKey: .iso_639_1) ?? ""
        name = try container.decodeIfPresent(String.self, forKey: .name) ?? ""
        englishName = try container.decodeIfPresent(String.self, forKey: .englishName) ?? ""
    }
}

public struct MovieVideo {
    public let iso_639_1: String
    public let iso_3166_1: String
    public let name: String
    public let key: String
    public let site: MovieVideoSite
    public let size: Int
    public let type: MovieVideoType
    public let official: Bool
    public let publishedAt: Date?
    public let id: String

    private enum CodingKeys: String, CodingKey {
        case iso_639_1, iso_3166_1
        case name, key, site
        case size, type, official, id
        case publishedAt = "published_at"
    }
}

extension MovieVideo: Decodable {
    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        iso_639_1 = try container.decodeIfPresent(String.self, forKey: .iso_639_1) ?? ""
        iso_3166_1 = try container.decodeIfPresent(String.self, forKey: .iso_3166_1) ?? ""
        name = try container.decodeIfPresent(String.self, forKey: .name) ?? ""
        key = try container.decodeIfPresent(String.self, forKey: .key) ?? ""
        site = MovieVideoSite(rawValue: try container.decodeIfPresent(String.self, forKey: .site) ?? "") ?? .unknown
        size = try container.decodeIfPresent(Int.self, forKey: .size) ?? 0
        type = MovieVideoType(rawValue: try container.decodeIfPresent(String.self, forKey: .type) ?? "") ?? .unknown
        official = try container.decodeIfPresent(Bool.self, forKey: .official) ?? false

        let publishedAtt = try container.decodeIfPresent(String.self, forKey: .publishedAt) ?? ""
        publishedAt = publishedAtt.toDate(input: .yyyyMMddTHHmmssZ)

        id = try container.decodeIfPresent(String.self, forKey: .id) ?? ""
    }
}

public struct MovieImages {
    public let backdrops: [MovieImage]
    public let logos: [MovieImage]
    public let posters: [MovieImage]

    private enum CodingKeys: String, CodingKey {
        case backdrops, logos, posters
    }
}

extension MovieImages: Decodable {
    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        backdrops = try container.decodeIfPresent([MovieImage].self, forKey: .backdrops) ?? []
        logos = try container.decodeIfPresent([MovieImage].self, forKey: .logos) ?? []
        posters = try container.decodeIfPresent([MovieImage].self, forKey: .posters) ?? []
    }
}

public struct MovieImage {
    public let aspectRatio: Double
    public let height: Int
    public let iso_639_1: String
    public let filePath: String
    public let voteAverage: Double
    public let voteCount: Int
    public let width: Int

    private enum CodingKeys: String, CodingKey {
        case aspectRatio = "aspect_ratio"
        case height, width, iso_639_1
        case filePath = "file_path"
        case voteAverage = "vote_average"
        case voteCount = "vote_count"
    }
}

extension MovieImage: Decodable {
    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        aspectRatio = try container.decodeIfPresent(Double.self, forKey: .aspectRatio) ?? 0.0
        height = try container.decodeIfPresent(Int.self, forKey: .height) ?? 0
        iso_639_1 = try container.decodeIfPresent(String.self, forKey: .iso_639_1) ?? ""
        filePath = try container.decodeIfPresent(String.self, forKey: .filePath) ?? ""
        voteAverage = try container.decodeIfPresent(Double.self, forKey: .voteAverage) ?? 0.0
        voteCount = try container.decodeIfPresent(Int.self, forKey: .voteCount) ?? 0
        width = try container.decodeIfPresent(Int.self, forKey: .width) ?? 0
    }
}

public struct MovieCredits {
    public let casts: [MovieCredit]
    public let crews: [MovieCredit]

    private enum CodingKeys: String, CodingKey {
        case crew, cast
    }
}

extension MovieCredits: Decodable {
    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        casts = try container.decodeIfPresent([MovieCredit].self, forKey: .cast) ?? []
        crews = try container.decodeIfPresent([MovieCredit].self, forKey: .crew) ?? []
    }
}

public struct MovieCredit {
    public let id: Int
    public let castId: Int
    public let creditId: String
    public let name: String
    public let profilePath: String
    public let character: String
    public let knownForDepartment: String
    public let department: String
    public let job: String

    private enum CodingKeys: String, CodingKey {
        case adult, gender, id
        case knownForDepartment = "known_for_department"
        case name
        case originalName = "original_name"
        case popularity
        case profilePath = "profile_path"
        case castId = "cast_id"
        case character
        case creditId = "credit_id"
        case order, department, job
    }
}

extension MovieCredit: Decodable {
    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        id = try container.decodeIfPresent(Int.self, forKey: .id) ?? 0
        castId = try container.decodeIfPresent(Int.self, forKey: .castId) ?? 0
        creditId = try container.decodeIfPresent(String.self, forKey: .creditId) ?? ""
        name = try container.decodeIfPresent(String.self, forKey: .name) ?? ""
        profilePath = try container.decodeIfPresent(String.self, forKey: .profilePath) ?? ""
        character = try container.decodeIfPresent(String.self, forKey: .character) ?? ""
        knownForDepartment = try container.decodeIfPresent(String.self, forKey: .knownForDepartment) ?? ""
        department = try container.decodeIfPresent(String.self, forKey: .department) ?? ""
        job = try container.decodeIfPresent(String.self, forKey: .job) ?? ""
    }
}
