//
//  MovieReviewRequest.swift
//  AssesMandiri
//
//  Created by Angga Setiawan on 09/02/24.
//

import Foundation

public struct MovieReviewRequest: Exportable {

    public let page: Int
    public let movieId: Int

    public init(page: Int, movieId: Int) {
        self.page = page
        self.movieId = movieId
    }

    public var asDict: [String: Any] {
        var result = [String: Any]()
        result["page"] = page
        return result
    }
}
