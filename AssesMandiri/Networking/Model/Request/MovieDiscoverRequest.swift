//
//  MovieDiscoverRequest.swift
//  AssesMandiri
//
//  Created by Angga Setiawan on 09/02/24.
//

import Foundation

public struct MovieDiscoverRequest: Exportable {

    public let page: Int
    public let genreId: Int

    public init(page: Int, genreId: Int) {
        self.page = page
        self.genreId = genreId
    }

    public var asDict: [String: Any] {
        var result = [String: Any]()
        result["page"] = page
        result["with_genres"] = genreId
        return result
    }
}
