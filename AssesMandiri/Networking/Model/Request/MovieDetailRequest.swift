//
//  MovieDetailRequest.swift
//  AssesMandiri
//
//  Created by Angga Setiawan on 09/02/24.
//

import Foundation

public enum MovieContent: String {
    case videos
    case images
    case credits
    case reviews
    case alternativeTitles = "alternative_titles"
    case similar
}

public struct MovieDetailRequest: Exportable {

    public let movieId: Int
    public let contents: [MovieContent]

    public init(movieId: Int, contents: [MovieContent]) {
        self.movieId = movieId
        self.contents = contents
    }

    public var asDict: [String: Any] {
        var result = [String: Any]()

        if !contents.isEmpty {
            result["append_to_response"] = contents.toString()
        }

        return result
    }
}

public extension [MovieContent] {
    func toString(separator: String = ",") -> String {
        return self.map { $0.rawValue }.joined(separator: separator)
    }
}
