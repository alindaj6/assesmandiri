//
//  Exportable.swift
//  AssesMandiri
//
//  Created by Angga Setiawan on 09/02/24.
//

import Foundation

public protocol Exportable {
    var asDict: [String: Any] { get }
}
