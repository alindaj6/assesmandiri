//
//  Dummy.swift
//  AssesMandiri
//
//  Created by Angga Setiawan on 09/02/24.
//

import Foundation

/// A dummy struct to present value of data which we actually do not care
public struct Dummy: Decodable {
}
