//
//  AppPagedResponse.swift
//  AssesMandiri
//
//  Created by Angga Setiawan on 09/02/24.
//

import Foundation

public struct AppPagedResponse<T: Decodable> {
    let currentPage: Int
    let nextPage: Int
    let hasNextPage: Bool

    let totalPages: Int
    let totalResults: Int

    let data: [T]?

    private enum CodingKeys: String, CodingKey {
        case page, total_pages
        case total_results
        case data = "results"
    }

    public init(currentPage: Int, nextPage: Int, hasNextPage: Bool, totalPages: Int, totalResults: Int, data: [T]?) {
        self.currentPage = currentPage
        self.nextPage = nextPage
        self.hasNextPage = hasNextPage
        self.totalPages = totalPages
        self.totalResults = totalResults
        self.data = data
    }
}

extension AppPagedResponse: Decodable {
    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)

        currentPage = try container.decodeIfPresent(Int.self, forKey: .page) ?? 0
        totalPages = try container.decodeIfPresent(Int.self, forKey: .total_pages) ?? 0
        totalResults = try container.decodeIfPresent(Int.self, forKey: .total_results) ?? 0

        if T.self != Dummy.self {
            do {
                data = try container.decodeIfPresent([T].self, forKey: .data)
                hasNextPage = !(currentPage == totalPages)
                nextPage = hasNextPage ? currentPage + 1 : currentPage
            } catch {
                data = nil
                hasNextPage = false
                nextPage = 1
                error.jsonParsingLog(model: T.self)
            }
        } else {
            data = nil
            hasNextPage = false
            nextPage = 1
        }
    }
}
