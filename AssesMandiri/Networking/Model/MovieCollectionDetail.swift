//
//  MovieCollectionDetail.swift
//  AssesMandiri
//
//  Created by Angga Setiawan on 09/02/24.
//

import Foundation

public struct MovieCollectionDetail {
    public let id: Int
    public let name: String
    public let overview: String
    public let parts: [Movie]

    private enum CodingKeys: String, CodingKey {
        case id, name, overview, parts
    }
}

extension MovieCollectionDetail: Decodable {
    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        id = try container.decodeIfPresent(Int.self, forKey: .id) ?? 0
        name = try container.decodeIfPresent(String.self, forKey: .name) ?? ""
        overview = try container.decodeIfPresent(String.self, forKey: .overview) ?? ""
        parts = try container.decodeIfPresent([Movie].self, forKey: .parts) ?? []
    }
}
