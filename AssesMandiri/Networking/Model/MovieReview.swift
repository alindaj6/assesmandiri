//
//  MovieReview.swift
//  AssesMandiri
//
//  Created by Angga Setiawan on 10/02/24.
//

import Foundation

public struct MovieReview {
    public let author: String
    public let authorDetails: AuthorDetails?
    public let createdAt: Date?
    public let updatedAt: Date?
    public let content, id: String
    public let url: URL?

    enum CodingKeys: String, CodingKey {
        case author
        case authorDetails = "author_details"
        case content
        case createdAt = "created_at"
        case id
        case updatedAt = "updated_at"
        case url
    }
}

extension MovieReview: Decodable {
    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        author = try container.decodeIfPresent(String.self, forKey: .author) ?? ""
        authorDetails = try container.decodeIfPresent(AuthorDetails.self, forKey: .authorDetails)
        content = try container.decodeIfPresent(String.self, forKey: .content) ?? ""

        let createdAtt = try container.decodeIfPresent(String.self, forKey: .createdAt) ?? ""
        createdAt = createdAtt.toDate(input: .yyyyMMddTHHmmssZ)

        id = try container.decodeIfPresent(String.self, forKey: .id) ?? ""

        let updatedAtt = try container.decodeIfPresent(String.self, forKey: .updatedAt) ?? ""
        updatedAt = updatedAtt.toDate(input: .yyyyMMddTHHmmssZ)

        url = URL(string: try container.decodeIfPresent(String.self, forKey: .url) ?? "")

    }
}

public struct AuthorDetails {
    public let name, username, avatarPath: String
    public let rating: Double

    enum CodingKeys: String, CodingKey {
        case name, username
        case avatarPath = "avatar_path"
        case rating
    }
}

extension AuthorDetails: Decodable {
    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        name = try container.decodeIfPresent(String.self, forKey: .name) ?? ""
        username = try container.decodeIfPresent(String.self, forKey: .username) ?? ""
        avatarPath = try container.decodeIfPresent(String.self, forKey: .avatarPath) ?? ""
        rating = try container.decodeIfPresent(Double.self, forKey: .rating) ?? 0.0
    }
}
