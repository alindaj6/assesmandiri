//
//  ApiProtocol.swift
//  AssesMandiri
//
//  Created by Angga Setiawan on 09/02/24.
//

import Foundation

enum APIMethod: String {
    case get, put, post, patch, delete
}

enum EncodingMethod {
    case jsonEncoding(params: [String: Any])
    case urlEncoding(params: [String: Any])
    case plain

    var requestParam: [String: Any]? {
        switch self {
        case .jsonEncoding(let params):
            return params
        case .urlEncoding(let params):
            return params
        case .plain:
            return nil
        }
    }
}

enum Authentication {
    case bearer(token: String)
    case apiKey(key: String)
    case none
}

enum MockMode {
    case local(behavior: MockBehavior)
    case api(behavior: MockBehavior)
    case never
}

enum MockBehavior {
    case immediate
    case delayed(seconds: TimeInterval)
}

public protocol VersionedApi {
    var version: String { get }
}

protocol ApiProtocol: VersionedApi {
    var method: APIMethod { get }

    var authorization: Authentication { get }

    var baseURL: String { get }

    var path: String { get }

    var headers: [String: String]? { get }

    var requestMethod: EncodingMethod { get }

    var mockMode: MockMode { get }

    var sampleData: Data { get }

    var timeout: DispatchTimeInterval { get }

    func asURLRequest() -> URLRequest
}

extension ApiProtocol {

    var authorization: Authentication { return .none }

    var baseURL: String { return MovieDBBaseURL.production.baseURL }

    var path: String { return "" }

    var headers: [String: String]? { return nil }

    var requestMethod: EncodingMethod { return .plain }

    var mockMode: MockMode { .never }

    var sampleData: Data { Data() }

    var version: String { return "3" }

    var timeout: DispatchTimeInterval { return .seconds(30) }

    func asURLRequest() -> URLRequest {
        var urlRequest = URLRequest(url: getURLBuilder().url!,
                                    cachePolicy: .reloadRevalidatingCacheData,
                                    timeoutInterval: timeout.toDouble)

        // HTTP Method
        urlRequest.httpMethod = method.rawValue.uppercased()

        // Append Header
        urlRequest = urlRequest.addHeader(getHeaders())

        // Request Body JSON
        if case .jsonEncoding(let params) = requestMethod {
            urlRequest.httpBody = params.data
        }

        // Logging Request
        urlRequest.log(requestParam: requestMethod.requestParam)

        return urlRequest
    }

    fileprivate func getBase() -> String {
        var base = baseURL
        if !version.isEmpty { base = base + "/" + version }
//        if !baseURL.isEmpty { base = base + "/" + baseURL }
        return base + "/" + path
    }

    fileprivate func getURLBuilder() -> URLComponents {
        var urlBuilder = URLComponents(string: getBase())!

        if case .urlEncoding(let params) = requestMethod {
            urlBuilder.queryItems = params.queryItems
        }

        if case .apiKey(let key) = authorization {
            let queryItem = URLQueryItem(name: "api_key", value: key)
            if (urlBuilder.queryItems?.append(queryItem)) == nil {
                urlBuilder.queryItems = [queryItem]
            }
        }

        return urlBuilder
    }

    fileprivate func getHeaders() -> [String: String] {
        var tempHeaders = [String: String]()

        tempHeaders["User-Agent"] = getUserAgent()
        tempHeaders["Content-Type"] = "application/json"
        tempHeaders["X-Device-Type"] = "iOS"

        if case .bearer(let token) = authorization {
            tempHeaders["Authorization"] = "Bearer \(token)"
        }

        if let headers {
            for header in headers {
                tempHeaders[header.key] = header.value
            }
        }

        return tempHeaders
    }

    fileprivate func getUserAgent() -> String {
        let info = Bundle.main.infoDictionary
        let appName = info?["CFBundleName"] as? String ?? "Unknown"
        let appVersion = info?["CFBundleShortVersionString"] as? String ?? "Unknown"
        let appBuild = info?["CFBundleVersion"] as? String ?? "Unknown"
        let appIdentifier = info?["CFBundleIdentifier"] as? String ?? "Unknown"

        let osNameVersion: String = {
            let version = ProcessInfo.processInfo.operatingSystemVersion
            let versionString = "\(version.majorVersion).\(version.minorVersion).\(version.patchVersion)"
            let osName: String = "iOS"
            return "\(osName) \(versionString)"
        }()

        return "\(appName)/\(appVersion) (\(appIdentifier); build:\(appBuild); \(osNameVersion))"
    }
}
