//
//  MovieDBBaseURL.swift
//  AssesMandiri
//
//  Created by Angga Setiawan on 09/02/24.
//

import Foundation

public enum MovieDBBaseURL: String, CaseIterable {
    private enum BaseURLConstant {
        case development
        case staging
        case hotfix
        case production

        var url: String {
            switch self {
            case .development: return "https://api.themoviedb.org"
            case .staging: return "https://api.themoviedb.org"
            case .hotfix: return "https://api.themoviedb.org"
            case .production: return "https://api.themoviedb.org"
            }
        }
    }

    case development
    case staging
    case hotfix
    case production

    public var baseURL: String {
        switch self {
        case .development:
            return BaseURLConstant.development.url
        case .staging:
            return BaseURLConstant.staging.url
        case .hotfix:
            return BaseURLConstant.hotfix.url
        case .production:
            return BaseURLConstant.production.url
        }
    }
}
