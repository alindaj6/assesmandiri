//
//  SSLPinningDelegate.swift
//  AssesMandiri
//
//  Created by Angga Setiawan on 09/02/24.
//

import Foundation

public enum SSLPinningHandlerType {
    case authenticated(credential: URLCredential)
    case cancel
    case `default`
}

// swiftlint:disable discouraged_direct_init
public class SSLPinningDelegate: NSObject, URLSessionDelegate {

    public func urlSession(_ session: URLSession, didReceive challenge: URLAuthenticationChallenge, completionHandler: @escaping (URLSession.AuthChallengeDisposition, URLCredential?) -> Void) {

        // Public Key Pinning
        switch self.getSSLPinning(challenge: challenge) {
        case .authenticated(let credential):
            completionHandler(.useCredential, credential)
        case .cancel:
            completionHandler(.cancelAuthenticationChallenge, nil)
        case .default:
            completionHandler(.performDefaultHandling, nil)
        }
    }

    public func configuration(timeout: DispatchTimeInterval = .seconds(30)) -> URLSessionConfiguration {
        let configuration = URLSessionConfiguration.ephemeral

        // as seconds, you can set your request timeout
        configuration.timeoutIntervalForRequest = timeout.toDouble

        // as seconds, you can set your resource timeout
        configuration.timeoutIntervalForResource = timeout.toDouble

        configuration.requestCachePolicy = .useProtocolCachePolicy

        return configuration
    }
}

extension SSLPinningDelegate {
    func getServerTrust(challenge: URLAuthenticationChallenge) -> SecTrust? {
        return challenge.protectionSpace.serverTrust
    }

    func getPublicKeysAndServerHashKeys(serverTrust: SecTrust, challenge: URLAuthenticationChallenge) -> (publicKeys: [String], serverHashKeys: [String]) {

        let publicKeys: [String] = Bundle().getSSLContent().publicKeys

        if publicKeys.isEmpty { return ([], []) }

        // Get Server Public Certificate
        let secCertificates = serverTrust.getSecCertificates

        // Convert Server Public Certificate to String of SHA256
        let serverHashKeys = secCertificates.getServerPublicKeys

        return (publicKeys, serverHashKeys)
    }

    func getSSLPinning(challenge: URLAuthenticationChallenge) -> SSLPinningHandlerType {

        let urlToCheck: [String] = Bundle().getSSLContent().urls

        let serverTrust: SecTrust? = self.getServerTrust(challenge: challenge)

        guard let serverTrust = serverTrust else {
            return .default
        }

        let publicKeysAndServerHashKeys = self.getPublicKeysAndServerHashKeys(serverTrust: serverTrust, challenge: challenge)
        let publicKeys = publicKeysAndServerHashKeys.publicKeys
        let serverHashKeys = publicKeysAndServerHashKeys.serverHashKeys

        if publicKeys.isEmpty { return .default }

        if serverHashKeys.isEmpty { return .default }

        let url = challenge.protectionSpace.host

        // Public Key Pinning
        if [url].contains(urlToCheck) {
            if publicKeys.contains(serverHashKeys) {
                let credential: URLCredential = URLCredential(trust: serverTrust)
                return .authenticated(credential: credential)
            } else {
                return .cancel
            }
        }

        return .default
    }
}
