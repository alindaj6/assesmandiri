//
//  ApiService.swift
//  AssesMandiri
//
//  Created by Angga Setiawan on 09/02/24.
//

import Foundation

struct APIService {

    static func request<T: Decodable>(for type: T.Type, route: ApiProtocol) async throws -> T {
        let urlRequest = route.asURLRequest()

        // This default value will be used for Mock Call/Mock Mode
        // If it is not a Mock Call/Mock Mode, Each value will be replace from Response API
        switch route.mockMode {
        case .local(let behavior):
            let response = URLResponse(url: urlRequest.url!, mimeType: nil,
                                       expectedContentLength: 0, textEncodingName: nil)
            var result = try await decodeFromData(for: T.self, data: route.sampleData)

            switch behavior {
            case .immediate:
                result = try await decodeFromData(for: T.self, data: route.sampleData)
            case .delayed(let seconds):
                try await Task.sleep(seconds: seconds)
                result = try await decodeFromData(for: T.self, data: route.sampleData)
            }

            response.log(request: urlRequest, data: route.sampleData, error: nil)
            return result
        case .api(let behavior):
            var apiCallResult: (data: Data, statusCode: Int)

            switch behavior {
            case .immediate:
                apiCallResult = try await callRequest(session: getSession(timeout: route.timeout),
                                                      urlRequest: urlRequest)
            case .delayed(let seconds):
                apiCallResult = try await callRequest(session: getSession(timeout: route.timeout),
                                                      urlRequest: urlRequest, delayed: seconds)
            }

            return try await decodeFromData(for: T.self, data: apiCallResult.data, statusCode: apiCallResult.statusCode)
        case .never:
            let apiCallResult = try await callRequest(session: getSession(timeout: route.timeout),
                                                      urlRequest: urlRequest)

            return try await decodeFromData(for: T.self, data: apiCallResult.data, statusCode: apiCallResult.statusCode)
        }
    }

    static fileprivate func getSession(timeout: DispatchTimeInterval = .seconds(30)) -> URLSession {
        return URLSession(configuration: SSLPinningDelegate().configuration(timeout: timeout),
                          delegate: SSLPinningDelegate(), delegateQueue: nil)
    }

    static fileprivate func callRequest(session: URLSession, urlRequest: URLRequest, delayed seconds: TimeInterval = 0) async throws -> (data: Data, statusCode: Int) {
        try await Task.sleep(seconds: seconds)
        let (data, response) = try await session.data(for: urlRequest)
        response.log(request: urlRequest, data: data)
        return (data, response.code)
    }

    static fileprivate func decodeFromData<T: Decodable>(for: T.Type, data: Data,
                                                         statusCode: Int = 200) async throws -> T {
        switch statusCode {
        case 200:
            return try data.toObject(for: T.self)
        default:
            throw APIError.badRequest(errorCode: statusCode)
        }
    }
}
