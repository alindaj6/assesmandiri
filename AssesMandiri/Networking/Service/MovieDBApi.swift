//
//  MovieDBApi.swift
//  AssesMandiri
//
//  Created by Angga Setiawan on 09/02/24.
//

import Foundation

enum MovieDBApi {
    case configuration

    case getMovieGenres
    case getDiscoverMovie(param: MovieDiscoverRequest)
    case getMovieDetail(params: MovieDetailRequest)
    case getCollection(id: Int)
    case getReviews(params: MovieReviewRequest)
}

// swiftlint:disable discouraged_direct_init
extension MovieDBApi: ApiProtocol {

    var authorization: Authentication {
        switch self {
        case .configuration:
            return .apiKey(key: "2beadee63eb9e175114bce81b0b3089c")
        default:
            return .bearer(token: "eyJhbGciOiJIUzI1NiJ9.eyJhdWQiOiIyYmVhZGVlNjNlYjllMTc1MTE0YmNlODFiMGIzMDg5YyIsInN1YiI6IjYxMzRjYjcwZWEzOTQ5MDA5NWNkMjdjYiIsInNjb3BlcyI6WyJhcGlfcmVhZCJdLCJ2ZXJzaW9uIjoxfQ.be8OidbWrs8M5LWMvSgBr2EhwfmO7VsIpDSL3r4IE0k")
        }
    }

    var version: String {
        return "3"
    }

    var path: String {
        switch self {
        case .configuration:
            return "configuration"
        case .getMovieGenres:
            return "genre/movie/list"
        case .getDiscoverMovie:
            return "discover/movie"
        case .getMovieDetail(let params):
            return "movie/\(params.movieId)"
        case .getCollection(let id):
            return "collection/\(id)"
        case .getReviews(let params):
            return "movie/\(params.movieId)/reviews"
        }
    }

    var method: APIMethod {
        return .get
    }

    var requestMethod: EncodingMethod {
        switch self {
        case .configuration: return .plain
        case .getMovieGenres: return .plain
        case .getDiscoverMovie(let param):
            return .urlEncoding(params: param.asDict)
        case .getMovieDetail(let params):
            return .urlEncoding(params: params.asDict)
        case .getCollection: return .plain
        case .getReviews(let params):
            return .urlEncoding(params: params.asDict)
        }
    }

    var mockMode: MockMode {
        return .never
    }

    var sampleData: Data {
        switch self {
        case .configuration:
            return Bundle().getMockDataFor(key: "Configuration").sampleData
        case .getMovieGenres:
            return Bundle().getMockDataFor(key: "Genres").sampleData
        case .getDiscoverMovie:
            return Bundle().getMockDataFor(key: "Discovers").sampleData
        case .getMovieDetail:
            return Bundle().getMockDataFor(key: "Detail").sampleData
        case .getCollection:
            return Bundle().getMockDataFor(key: "Collection").sampleData
        case .getReviews:
            return Bundle().getMockDataFor(key: "Reviews").sampleData
        }
    }
}
