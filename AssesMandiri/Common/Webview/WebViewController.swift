//
//  WebViewController.swift
//  AssesMandiri
//
//  Created by Angga Setiawan on 10/02/24.
//

import UIKit
import WebKit

protocol WebViewControllerDelegate: AnyObject {
    func didStart(vc: UIViewController, url: String)
    func didOpen(vc: UIViewController, url: String)
}

class WebViewController: UIViewController {

    private static let defaultHtmlStyle = """
<header>
    <meta
        name='viewport'
        content='width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no' />
    <style type=\"text/css\">
        body { font-family: '-apple-system','HelveticaNeue'; font-size:17; }
    </style>
</header>
"""

    private lazy var webView: WKWebView = {
        let prefs = WKWebpagePreferences()
        prefs.allowsContentJavaScript = true
        let config = WKWebViewConfiguration()
        config.defaultWebpagePreferences = prefs
        let webView = WKWebView(frame: self.view.bounds, configuration: config)
        webView.translatesAutoresizingMaskIntoConstraints = false
        webView.navigationDelegate = self
        return webView
    }()

    private lazy var activityIndicator: UIActivityIndicatorView = {
        let view = UIActivityIndicatorView(style: .medium)
        view.frame = CGRect(x: 0, y: 0, width: 20, height: 20)
        view.hidesWhenStopped = true

        let barButton = UIBarButtonItem(customView: view)
        self.navigationItem.setRightBarButton(barButton, animated: true)
        return view
    }()

    private var source: WebSource!
    private var transformer: WebSourceTransformer!
    weak var delegate: WebViewControllerDelegate?

    static func create(source: WebSource, transformer: WebSourceTransformer) -> WebViewController {
        let vc = WebViewController()
        vc.source = source
        vc.transformer = transformer
        return vc
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isTranslucent = false
        self.hideTabBar()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.addSubview(webView)
        self.hideTabBar()
        webView.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }

        switch self.source! {
        case .url(let title, let origin, _):
            self.title = title
            self.show(loading: true)
            transformer.transform(origin: origin) { [weak self] (transformed) in
                self?.show(loading: false)
                if let url = transformed.url {
                    let requestObj = URLRequest(url: url)
                    self?.webView.load(requestObj)
                }
            }
        case .html(let title, let content):
            self.title = title
            webView.loadHTMLString(WebViewController.defaultHtmlStyle + content, baseURL: nil)
        }
    }
}

extension WebViewController: WKNavigationDelegate {

    func webView(_ webView: WKWebView, didCommit navigation: WKNavigation!) {
        self.show(loading: true)
    }

    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        self.show(loading: false)
    }

    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        self.show(loading: false)
    }

    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        self.delegate?.didStart(vc: self, url: webView.url?.absoluteString ?? "")
        self.delay(interval: .seconds(1)) {
            self.delegate?.didOpen(vc: self, url: webView.url?.absoluteString ?? "")
        }
    }
}

extension WebViewController {
    func show(loading: Bool) {
        if loading {
            activityIndicator.startAnimating()
//            UIApplication.shared.isNetworkActivityIndicatorVisible = true
        } else {
            activityIndicator.stopAnimating()
//            UIApplication.shared.isNetworkActivityIndicatorVisible = false
        }
    }
}
