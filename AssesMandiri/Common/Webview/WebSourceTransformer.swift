//
//  WebSourceTransformer.swift
//  AssesMandiri
//
//  Created by Angga Setiawan on 10/02/24.
//

import UIKit

protocol WebSourceTransformer {

    func transform(origin: String, completion: @escaping (String) -> Void)
}

struct PlainTransformer: WebSourceTransformer {

    func transform(origin: String, completion: @escaping (String) -> Void) {
        completion(origin)
    }
}
