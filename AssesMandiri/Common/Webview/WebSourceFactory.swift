//
//  WebSourceFactory.swift
//  AssesMandiri
//
//  Created by Angga Setiawan on 10/02/24.
//

import UIKit
import SafariServices

enum WebSource {

    case url(title: String?, origin: String, preferred: String?)
    case html(title: String?, content: String)
}

enum WebSourceNavigation {

    case webview(transformer: WebSourceTransformer)
    case safari(useSystem: Bool)
}

struct WebSourceFactory {

    @discardableResult
    static func startNavigation(with source: WebSource, using navigation: WebSourceNavigation = .safari(useSystem: false)) -> Bool {
        switch source {
        case .url(_, let origin, let preferred):
            if let preferred = preferred, let preferredUrl = preferred.url,
                UIApplication.shared.canOpenURL(preferredUrl) {
                UIApplication.shared.open(preferredUrl, options: [:], completionHandler: nil)
            }

            switch navigation {
            case .webview(let transformer):
                let vc = WebViewController.create(source: source, transformer: transformer)
                UINavigationController.topMostVc?.navigationController?.pushViewController(vc, animated: true)
                return true
            case .safari(let useSystem):
                if let url = origin.checkURLSchemeSupported.urlString.url {
                    if useSystem {
                        // Use External System Browser (Safari on iOS)
                        UIApplication.shared.open(url, options: [:], completionHandler: nil)
                    } else {
                        // Use Internal Safari Controller
                        let safariVc = SFSafariViewController(url: url)
                        UINavigationController.topMostVc?.present(safariVc, animated: true, completion: nil)
                    }
                    return true
                }
            }
        case .html:
            let vc = WebViewController.create(source: source, transformer: PlainTransformer())
            UINavigationController.topMostVc?.navigationController?.pushViewController(vc, animated: true)
            return true
        }

        return false
    }
}
