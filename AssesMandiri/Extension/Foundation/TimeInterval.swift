//
//  TimeInterval.swift
//  AssesMandiri
//
//  Created by Angga Setiawan on 09/02/24.
//

import Foundation

public extension DispatchTimeInterval {
    var toDouble: Double {
        var result: Double = 0

        switch self {
        case .seconds(let value):
            result = value.toDouble
        case .milliseconds(let value):
            result = value.toDouble * 0.001
        case .microseconds(let value):
            result = value.toDouble * 0.000001
        case .nanoseconds(let value):
            result = value.toDouble * 0.000000001
        default:
            result = 0
        }

        return result
    }
}
