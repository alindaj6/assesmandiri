//
//  String.swift
//  AssesMandiri
//
//  Created by Angga Setiawan on 09/02/24.
//

import Foundation

extension String {
    var trailingSpacesTrimmed: String {
        var newString = self
        while newString.last?.isWhitespace == true {
            newString = String(newString.dropLast())
        }
        return newString
    }

    var sampleData: Data {
        return self.data(using: .utf8) ?? Data()
    }

    func toDate(input: DateFormatterPrefStyle.Input) -> Date? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = input.rawValue
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        dateFormatter.timeZone = TimeZone.current
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        let date = dateFormatter.date(from: self)
        return date
    }

    func add(_ limiter: Character, forEvery at: Int) -> String {
        var stringtoConvert = self
        if stringtoConvert.count <= at { return stringtoConvert }
        for i in stride(from: stringtoConvert.count, to: 0, by: (at * -1)) where i != stringtoConvert.count {
            stringtoConvert.insert(limiter,
                                   at: stringtoConvert.index(stringtoConvert.startIndex, offsetBy: i))
        }
        return stringtoConvert
    }

    var currency: String {
        let text = self.replacingOccurrences(of: ".", with: "")
        return "$\(text.add(Character("."), forEvery: 3))"
    }
}

public extension String.SubSequence {
    var string: String {
        return String(self)
    }
}

public extension StringProtocol where Index == String.Index {
    func nsRange(from range: Range<Index>) -> NSRange {
        return NSRange(range, in: self)
    }
}

public extension String {
    var checkURLSchemeSupported: (Bool, urlString: String) {
        if self.checkURLhasHTTPS { return (true, self.replacingOccurrences(of: " ", with: "%20")) }
        if self.checkURLhasHTTP { return (true, self.replacingOccurrences(of: " ", with: "%20")) }
        var urlString = self
        if !self.lowercased().hasPrefix("http://") {
            urlString = "http://\(urlString)"
        }
        return (false, "\(urlString.replacingOccurrences(of: " ", with: "%20"))")
    }

    var checkURLhasHTTPS: Bool {
        return self.lowercased().contains("https")
    }

    var checkURLhasHTTP: Bool {
        return self.lowercased().contains("http")
    }

    var url: URL? {
        return URL(string: self)
    }
}
