//
//  Date.swift
//  AssesMandiri
//
//  Created by Angga Setiawan on 09/02/24.
//

import Foundation

extension Date {
    var getDay: String {
        var dateString = ""

        if self == Date() {
            let time = self.toString(input: .yyyyMMddHHmmssZZZZ, output: .HHmm) ?? ""
            dateString = "Hari Ini, \(time)"
        } else {
            let datee = self.toString(input: .yyyyMMddHHmmssZZZZ, output: .ddMMMyyyy) ?? ""
            dateString = "\(self.day), \(datee)"
        }

        return dateString
    }

    func toString(input: DateFormatterPrefStyle.Input = .yyyyMMddHHmmss,
                  output: DateFormatterPrefStyle.Output = .ddMMyyyy) -> String? {

        let formatter = DateFormatter()

        // initially set the format based on your datepicker date / server String
        formatter.dateFormat = input.rawValue

        // convert your string to date
        let yourDate = formatter.date(from: formatter.string(from: self))

        // then again set the date format whhich type of output you need
        formatter.dateFormat = output.rawValue

        guard let dateString = yourDate else { return nil }
        return formatter.string(from: dateString)
    }

    var day: String {
        // returns an integer from 1 - 7, with 1 being Sunday and 7 being Saturday
        if let value = Calendar.current.dateComponents([.weekday], from: self).weekday {
            switch value {
            case 1:
                return "Sunday"
            case 2:
                return "Monday"
            case 3:
                return "Tuesday"
            case 4:
                return "Wednesday"
            case 5:
                return "Thursday"
            case 6:
                return "Friday"
            case 7:
                return "Saturday"
            default:
                return ""
            }
        }
        return ""
    }

    var getNetworkLoggerTime: String {
        return "\(self.day), \(self.toString(input: .yyyyMMddHHmmssZZZZ, output: .ddMMMMyyyyHHmmss) ?? "")"
    }
}

public struct DateFormatterPrefStyle {

    public enum Input: String {

        case ddMMMyyyy = "dd MMM yyyy"

        case ddMMyyyy = "dd-MM-yyyy"
        case yyyyMMdd = "yyyy-MM-dd"
        case yyyyMMddHHmm = "yyyy-MM-dd HH:mm"
        case yyyyMMddHHmmss = "yyyy-MM-dd HH:mm:ss"
        case MMMddyyyy = "MMM dd, yyyy"
        case yyyyMMddHHmmssZ = "yyyy-MM-dd HH:mm:ss Z"
        case yyyyMMddHHmmssZZZZ = "yyyy-MM-dd HH:mm:ss ZZZZ"
        case yyyyMMddhhmmssaZZZZ = "yyyy-MM-dd hh:mm:ss a zzzz"
        case yyyyMMddTHHmmssZ = "yyyy-MM-dd'T'HH:mm:ssZ"

        case HHmm = "HH:mm"

        case MMMMyyyy = "MMMM-yyyy"
        case yyyyMMMMdd = "yyyy-MMMM-dd"
    }

    public enum Output: String {

        case d = "d"
        case dd = "dd"
        case EEEE = "EEEE"
        case M = "M"
        case yyyy = "yyyy"
        case MMMdd = "MMM dd"
        case MMdd = "MM-dd"
        case ddMMM = "dd MMM"
        case MMMyyyy = "MMM yyyy"
        case MMyyyy = "MM-yyyy"
        case yyyyMMddSlash = "yyyy/MM/dd"
        case yyyyMMdd = "yyyy-MM-dd"
        case ddMMMyyyy = "dd MMM yyyy"
        case ddMMyyyy = "dd-MM-yyyy"
        case ddMMyyyyHHmmss = "dd-MM-yyyy HH:mm:ss"
        case yyyyMMddHHmm = "yyyy-MM-dd HH:mm"
        case yyyyMMddHHmmss = "yyyy-MM-dd HH:mm:ss"
        case yyyyMMddHHmmssZZZZ = "yyyy-MM-dd HH:mm:ss ZZZZ"
        case HHmma = "HH:mm a"
        case MMMdyyyy = "MMM d, yyyy"
        case HHmm = "HH:mm"
        case HHmm_V2 = "HH.mm"
        case hhmma = "hh:mm a"
        case EEEEdMMMyyyyhhmma = "EEEE, d MMM yyyy | hh:mm a"
        case EEEEdMMMyyyy = "EEEE, d MMM yyyy"
        case EEEEdMMMMyyyy = "EEEE, d MMMM yyyy"
        case MMMddyyyyHHmm = "MMM dd, yyyy | HH.mm"
        case MMMddyyyyHHmma = "MMM dd, yyyy | HH.mm a"
        case HHmmss = "HH:mm:ss"
        case ddMMMMyyyy = "dd MMMM yyyy"
        case ddMMMMyyyyHHmmss = "dd MMMM yyyy HH:mm:ss"
        case MMMddyyyy = "MMM dd, yyyy"
        case mss = "m:ss"
        case ddMMyyyy_SLASH = "dd/MM/yyyy"
        case ddMMMMyyyyHHmma = "dd MMMM yyyy | HH.mm a"

        case MMMMddyyyy = "MMMM dd, yyyy 'at'"
        case MMMMddyyyyHHmm = "MMMM dd, yyyy 'at' HH:mm"
        case mmss = "mm:ss"
        case ddMyyyy = "dd/M/yyyy"

        case dMMMMyyyyhhmma = "d MMMM yyyy\nhh.mm a"
        case ddMMMyyyyhhmmm = "dd MMM yyyy | hh.mm"
        case ddMMMyyyyHHmmss = "dd MMM yyyy HH:mm:ss"

        case MMyyyy_SLASH = "MM/yyyy"
        case MMMMyyyy = "MMMM yyyy"
        case MMMM = "MMMM"

        case ddMMMyyyyHHmm = "dd MMM yyyy, HH:mm"
    }
}
