//
//  Dictionary.swift
//  AssesMandiri
//
//  Created by Angga Setiawan on 09/02/24.
//

import Foundation

extension [String: Any] {
    var queryItems: [URLQueryItem] {
        return self.map {
            // Swift 4
            URLQueryItem(name: $0.0, value: "\($0.1)")
        }
    }

    var data: Data? {
        do {
            return try JSONSerialization.data(withJSONObject: self)
        } catch {
            assertionFailure(error.localizedDescription)
            return nil
        }
    }
}

extension Dictionary {
    var prettyPrintedJSON: String? {
        do {
            let data: Data = try JSONSerialization.data(withJSONObject: self, options: .prettyPrinted)
            return String(data: data, encoding: .utf8)?.replacingOccurrences(of: "\\/", with: "/")
        } catch {
            print("dictionary to pretty print json fail --> \(error.localizedDescription)")
            return nil
        }
    }
}

extension Dictionary {
    fileprivate func merge(dict: [Key: Value]) -> [Key: Value] {
        var mutableCopy = self
        for (key, value) in dict {
            // If both dictionaries have a value for same key, the value of the other dictionary is used.
            mutableCopy[key] = value
        }
        return mutableCopy
    }

    func merges(dictionaries: [Dictionary<Key, Value>]) -> [Key: Value] {
        if dictionaries.isEmpty {
            return [Key: Value]()
        }

        var firstDict = self
        for dict in dictionaries {
            firstDict = firstDict.merge(dict: dict)
        }

        return firstDict
    }
}
