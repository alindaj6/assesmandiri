//
//  Security.swift
//  AssesMandiri
//
//  Created by Angga Setiawan on 09/02/24.
//

import Foundation

public extension SecTrust {
    var getSecCertificates: [SecCertificate] {
        var certs: [SecCertificate] = []
        for i in 0...SecTrustGetCertificateCount(self) - 1 {
            if let cert = SecTrustGetCertificateAtIndex(self, i) {
                certs.append(cert)
            }
        }
        return certs
    }
}

public extension SecCertificate {
    var getServerPublicKey: String {
        if let serverPublicKey = SecCertificateCopyKey(self),
            let serverPublicKeyData = SecKeyCopyExternalRepresentation(serverPublicKey, nil) {
            let data: Data = serverPublicKeyData as Data
            let serverHashKey = data.toSHA256
            return serverHashKey
        }
        return ""
    }
}

public extension [SecCertificate] {
    var getServerPublicKeys: [String] {
        var serverPublicKeys: [String] = []
        for certificate in self {
            serverPublicKeys.append(certificate.getServerPublicKey)
        }
        return serverPublicKeys
    }
}
