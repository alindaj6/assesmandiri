//
//  Int.swift
//  AssesMandiri
//
//  Created by Angga Setiawan on 09/02/24.
//

import Foundation

extension Int {
    var toDouble: Double {
        return Double(self)
    }

    var toString: String {
        return String(self)
    }

    var toCurrency: String {
        return self.toString.currency
    }
}
