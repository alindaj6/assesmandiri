//
//  URLResponse.swift
//  AssesMandiri
//
//  Created by Angga Setiawan on 09/02/24.
//

import Foundation

extension URLResponse {

    func log(request: URLRequest, data: Data?, decodable: Decodable? = nil, error: Error? = nil, isPrintJSON: Bool = true) {
        print("""

        ======================================== Response ========================================
        Response Time: \(Date().getNetworkLoggerTime)
        Response Endpoint: \(self.url?.absoluteString ?? "")
        Response Status Code: \(self.code)
        Response Error: \(error?.localizedDescription ?? "")

        Response Header: \n\(self.headerFields?.prettyPrintedJSON ?? "")

        Response: \n\(isPrintJSON ? data?.prettyPrintedJSON ?? "" : "")
        ======================================== Response ========================================

        """)
    }

    var httpResponse: HTTPURLResponse? {
        return self as? HTTPURLResponse
    }

    var code: Int {
        return self.httpResponse?.statusCode ?? 0
    }

    var headerFields: [String: Any]? {
        return self.httpResponse?.allHeaderFields as? [String: Any]
    }
}
