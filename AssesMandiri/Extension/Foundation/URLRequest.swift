//
//  URLRequest.swift
//  AssesMandiri
//
//  Created by Angga Setiawan on 09/02/24.
//

import Foundation

extension URLRequest {
    func log(requestParam: [String: Any]?) {
        print("""

        ======================================== Request ========================================
        Request Time: \(Date().getNetworkLoggerTime)
        Request Endpoint: \(self.url?.absoluteString ?? "")
        Request Method: \(self.httpMethod ?? "")

        Request Headers: \n\((self.allHTTPHeaderFields ?? [:]).prettyPrintedJSON ?? "")

        Request: \n\(requestParam?.prettyPrintedJSON ?? "")
        ======================================== Request ========================================

        """)
    }
}

extension URLRequest {
    mutating func addHeader(_ headers: [String: String]) -> URLRequest {
        for header in headers {
            self.addValue(header.value, forHTTPHeaderField: header.key)
        }
        return self
    }
}
