//
//  Bundle.swift
//  AssesMandiri
//
//  Created by Angga Setiawan on 09/02/24.
//

import Foundation

extension Bundle {
    func getMockDataFor(key: String) -> String {
        if let mockDatas = getContentFilePlist(forType: Dictionary<String, String>.self, name: "MockData") {
            for (keyInData, value) in mockDatas where keyInData == key {
                return value
            }
        }

        return ""
    }

    func getSSLContent() -> (urls: [String], publicKeys: [String]) {
        var urls: [String] = []
        var publicKeys: [String] = []

        if let sslFromPlist = getContentFilePlist(forType: Dictionary<String, [String]>.self, name: "SSLContent") {
            for (keyInData, value) in sslFromPlist {
                if keyInData == "urls" {
                    urls = value
                } else if keyInData == "publicKeys" {
                    publicKeys = value
                }
            }
        }

        return (urls, publicKeys)
    }

    func getContentFilePlist<T>(forType: T.Type, name: String) -> T? {
        guard let path = Bundle.main.path(forResource: name, ofType: "plist") else { return nil }
        return NSDictionary(contentsOfFile: path) as? T
    }
}
