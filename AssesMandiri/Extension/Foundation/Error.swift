//
//  Error.swift
//  AssesMandiri
//
//  Created by Angga Setiawan on 09/02/24.
//

import Foundation

enum APIError: Error {
    case badRequest(errorCode: Int)
    case decodingError
}

public extension Error {
    var nsError: NSError {
        return self as NSError
    }

    func jsonParsingLog<T: Decodable>(model: T.Type) {
        self.logDecoding()
        var keysResponse: String = ""
        if let codingKeys = self.nsError.userInfo["NSCodingPath"] as? [CodingKey] {
            keysResponse = codingKeys.keys
        }
        print("Model: \(T.self)\nKeys: \(keysResponse)\nDescription: \(self.nsDebugDesc)")
    }

    func jsonParsingLog() {
        self.logDecoding()
        var keysResponse: String = ""
        if let codingKeys = self.nsError.userInfo["NSCodingPath"] as? [CodingKey] {
            keysResponse = codingKeys.keys
        }
        print("Keys: \(keysResponse)\nDescription: \(self.nsDebugDesc)")
    }

    func logDecoding() {
        print("JSON Parsing Error --> \(self.localizedDescription)")
        print("ERROR DECODING --> \(self.nsError.userInfo)")
    }

    var nsDebugDesc: String {
        return self.nsError.userInfo["NSDebugDescription"] as? String ?? ""
    }
}

extension [CodingKey] {
    var keys: String {
        let charToAppend: String = " --> "
        var array: [String] = []

        for content in self {
            array.append(content.stringValue)
        }

        return array.joined(separator: charToAppend)
    }
}
