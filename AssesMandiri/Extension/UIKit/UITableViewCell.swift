//
//  UITableViewCell.swift
//  AssesMandiri
//
//  Created by Angga Setiawan on 09/02/24.
//

import UIKit

extension UITableViewCell {
    static func identifier() -> String {
        return String(describing: self)
    }
}
