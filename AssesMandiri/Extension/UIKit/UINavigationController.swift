//
//  UINavigationController.swift
//  AssesMandiri
//
//  Created by Angga Setiawan on 10/02/24.
//

import UIKit

public extension UINavigationController {

    static var topMostVc: UIViewController? {
        var topController: UIViewController?
        if #available(iOS 13.0, *) {
            topController = UIApplication.shared.windows.first?.rootViewController
        } else {
            // Code for earlier iOS versions
            topController = UIApplication.shared.keyWindow?.rootViewController
        }
        while topController?.presentedViewController != nil {
            topController = topController?.presentedViewController
        }
        if let tab = topController as? UITabBarController {
            topController = tab.selectedViewController
        }
        if let navigation = topController as? UINavigationController {
            topController = navigation.visibleViewController
        }
        return topController
    }

    // hide navigation bar
    func hide() {
        self.setNavigationBarHidden(true, animated: false)
    }

    // show navigation bar
    func show() {
        self.setNavigationBarHidden(false, animated: false)
    }
}
