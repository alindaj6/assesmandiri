//
//  UILabel.swift
//  AssesMandiri
//
//  Created by Angga Setiawan on 09/02/24.
//

import UIKit

public struct TextFocus {
    let text: String
    let color: UIColor?
    let font: UIFont?
    let underlineStyle: Bool
    let onlyInIndex: Int?

    // onlyInIndex: only implement attribute to index-n
    // when "text" found > 1
    public init(text: String, color: UIColor? = nil, font: UIFont? = nil,
                underlineStyle: Bool = false, onlyInIndex: Int? = nil) {
        self.text = text
        self.color = color
        self.font = font
        self.underlineStyle = underlineStyle
        self.onlyInIndex = onlyInIndex
    }
}

public extension UILabel {
    func setText(_ targetStr: String, withFocuses focuses: [TextFocus]) {
        guard focuses.count > 0 else {
            self.text = targetStr
            return
        }

        let attributeStr = NSMutableAttributedString(string: targetStr)

        for focus in focuses {
            let rangeIndexes = targetStr.ranges(of: focus.text)

            rangeIndexes.enumerated().forEach { (index, rangeIndex) in
                if let onlyInIndex = focus.onlyInIndex {
                    if index == onlyInIndex {
                        let nsRange = targetStr.nsRange(from: rangeIndex)
                        let attributes = self.getAttributes(targetStr: targetStr,
                                                              focus: focus)
                        attributeStr.addAttributes(attributes, range: nsRange)
                    }
                } else {
                    let nsRange = targetStr.nsRange(from: rangeIndex)
                    let attributes = self.getAttributes(targetStr: targetStr,
                                                        focus: focus)
                    attributeStr.addAttributes(attributes, range: nsRange)
                }
            }
        }
        self.attributedText = attributeStr
    }

    fileprivate func getAttributes(targetStr: String, focus: TextFocus) -> [NSAttributedString.Key: Any] {
        var attributes = [NSAttributedString.Key: Any]()

        if let focusColor = focus.color {
            attributes[.foregroundColor] = focusColor
        }

        if let focusedFont = focus.font {
            attributes[.font] = focusedFont

            if focus.underlineStyle {
                attributes[.underlineStyle] = NSUnderlineStyle.single.rawValue
            }
        }

        return attributes
    }

    func setText(_ text: String, andImage image: UIImage, afterText isAfter: Bool, bounds: CGRect? = nil) {
        self.text = text
        let attachment = NSTextAttachment()
        attachment.image = image
        if let bounds = bounds {
            attachment.bounds = bounds
        }
        let attachmentString = NSAttributedString(attachment: attachment)
        if isAfter {
            let strLabelText = NSMutableAttributedString(string: "\(text) ")
            strLabelText.append(attachmentString)
            self.attributedText = strLabelText
        } else {
            let strLabelText = NSAttributedString(string: " \(text)")
            let mutableAttachmentString = NSMutableAttributedString(attributedString: attachmentString)
            mutableAttachmentString.append(strLabelText)
            self.attributedText = mutableAttachmentString
        }
    }

    var numberOfLinesFitContent: Int {
        let maxSize = CGSize(width: frame.size.width, height: CGFloat(Float.infinity))
        let charSize = font.lineHeight
        let text = (self.text ?? "") as NSString
        let textSize = text.boundingRect(
            with: maxSize, options: .usesLineFragmentOrigin, attributes: [.font: font as Any], context: nil)
        return Int(ceil(textSize.height/charSize))
    }
}
