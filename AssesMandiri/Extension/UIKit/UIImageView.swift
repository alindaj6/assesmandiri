//
//  UIImageView.swift
//  AssesMandiri
//
//  Created by Angga Setiawan on 09/02/24.
//

import UIKit
import Kingfisher

public enum DefaultImage {
    case custom(image: UIImage)
    case unknown

    var image: UIImage {
        switch self {
        case .custom(let image):
            return image
        case .unknown:
            return UIImage()
        }
    }
}

public extension UIImageView {

    func setOriginalImage(path: String, placeholder: DefaultImage, rounded: CGFloat = 0) {
        setImage(with: URL(string: "https://image.tmdb.org/t/p/original/\(path)"), placeholder: placeholder, rounded: rounded)
    }

    func setImage(with url: URL?, placeholder: DefaultImage, rounded: CGFloat = 0) {
        guard let url = url else {
            self.image = placeholder.image
            return
        }

        let cache = ImageCache.default
        cache.memoryStorage.config.expiration = .seconds(10)

        self.kf.indicatorType = .activity
        var processors: [KingfisherOptionsInfoItem] = [.cacheSerializer(FormatIndicatedCacheSerializer.png)]

        let roundCornerImageProcessor = RoundCornerImageProcessor(cornerRadius: rounded)

        if rounded != 0 { processors.append(.processor(roundCornerImageProcessor)) }

        self.kf.setImage(with: url, placeholder: placeholder.image, options: processors)
    }
}
