//
//  UIViewController.swift
//  AssesMandiri
//
//  Created by Angga Setiawan on 09/02/24.
//

import UIKit

public extension UIViewController {
    func showTabBar() {
        if let tabBarController = self.tabBarController {
            if tabBarController.tabBar.isHidden {
                tabBarController.tabBar.isHidden = false
            } else if tabBarController.tabBar.alpha == 0.0 {
                UIView.animate(withDuration: 0.1) {
                    tabBarController.tabBar.alpha = 1.0
                }
            }
        }
    }

    func hideTabBar() {
        if let tabBarController = self.tabBarController {
            if !tabBarController.tabBar.isHidden {
                tabBarController.tabBar.isHidden = true
            }
        }
    }
}

public extension UIViewController {
    func delay(interval: DispatchTimeInterval, completion: @escaping () -> Void) {
        DispatchQueue.main.asyncAfter(deadline: .now() + interval) {
            completion()
        }
    }
}
