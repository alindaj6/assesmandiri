//
//  CALayer.swift
//  AssesMandiri
//
//  Created by Angga Setiawan on 10/02/24.
//

import UIKit

public extension CALayer {
    func applyCornerRadiusShadow(color: UIColor = .black, alpha: Float = 0.1,
                                 x: CGFloat = 0, y: CGFloat = 2, blur: CGFloat = 4,
                                 spread: CGFloat = 0, cornerRadiusValue: CGFloat = 5) {
        cornerRadius = cornerRadiusValue
        shadowColor = color.cgColor
        shadowOpacity = alpha
        shadowOffset = CGSize(width: x, height: y)
        shadowRadius = blur / 2.0
        if spread == 0 {
            shadowPath = nil
        } else {
            let dx = -spread
            let rect = bounds.insetBy(dx: dx, dy: dx)
            shadowPath = UIBezierPath(rect: rect).cgPath
        }
    }

    func applyCornerRadiusShadow(cornerRadiusValue: CGFloat = 5) {
        let color: UIColor = .black
        let alpha: Float = 0.1
        let x: CGFloat = 0
        let y: CGFloat = 2
        let blur: CGFloat = 4
        let spread: CGFloat = 0

        cornerRadius = cornerRadiusValue
        shadowColor = color.cgColor
        shadowOpacity = alpha
        shadowOffset = CGSize(width: x, height: y)
        shadowRadius = blur / 2.0
        if spread == 0 {
            shadowPath = nil
        } else {
            let dx = -spread
            let rect = bounds.insetBy(dx: dx, dy: dx)
            shadowPath = UIBezierPath(rect: rect).cgPath
        }
    }
}
