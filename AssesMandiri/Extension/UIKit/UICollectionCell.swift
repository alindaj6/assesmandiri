//
//  UICollectionCell.swift
//  AssesMandiri
//
//  Created by Angga Setiawan on 09/02/24.
//

import UIKit

public extension UICollectionViewCell {
    static func identifier() -> String {
        return String(describing: self)
    }
    static func nib() -> UINib {
        return UINib(nibName: identifier(), bundle: nil)
    }
}
