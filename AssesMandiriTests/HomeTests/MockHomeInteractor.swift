//
//  MockHomeInteractor.swift
//  AssesMandiriTests
//
//  Created by Angga Setiawan on 10/02/24.
//

import Foundation
@testable import AssesMandiri

class MockHomeInteractor: HomeInteractorInterface {

    var invokedDatasGetter = false
    var invokedDatasGetterCount = 0
    var stubbedDatas: [MovieGenre] = []

    func getGenres() async -> [AssesMandiri.MovieGenre] {
        invokedDatasGetter = true
        invokedDatasGetterCount = 1
        return stubbedDatas
    }
}
