//
//  MockHomeRouter.swift
//  AssesMandiriTests
//
//  Created by Angga Setiawan on 10/02/24.
//

import Foundation
@testable import AssesMandiri

class MockHomeRouter: HomeRouterInterface {
    func navigateToDiscover(from view: AssesMandiri.HomeViewInterface, genre: AssesMandiri.MovieGenre) { }
}
