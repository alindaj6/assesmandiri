//
//  HomeTests.swift
//  AssesMandiriTests
//
//  Created by Angga Setiawan on 10/02/24.
//

import Nimble
import XCTest

import Foundation
@testable import AssesMandiri

// swiftlint:disable force_try discouraged_direct_init
class HomeTests: XCTestCase {
    private var homeInteractor: MockHomeInteractor!
    private var homeRouter: MockHomeRouter!
    private var homeView: MockHomeView!
    private var homePresenter: HomePresenterInterface!

    override func setUpWithError() throws {
        try super.setUpWithError()
        homeInteractor = MockHomeInteractor()
        homeRouter = MockHomeRouter()
        homeView = MockHomeView()
        homePresenter = HomePresenter(view: homeView, router: homeRouter, interactor: homeInteractor)
    }

    override func tearDownWithError() throws {
        try super.tearDownWithError()
        homePresenter = nil
        homeInteractor = nil
        homeRouter = nil
        homeView = nil
    }

    func testNotifyViewDidLoad() {
        homePresenter.notifyViewDidLoad()
        expect(self.homeView.invokedSetupView).to(beTrue())
        expect(self.homeView.invokedSetTitle).to(beTrue())
        expect(self.homeView.invokedTitle).to(equal("Home"))
    }

    func testGetGenreBy() {
        expect(self.homePresenter.getGenreBy(0)).to(beNil())
        homeInteractor.stubbedDatas = try! Bundle().getMockDataFor(key: "Genres").sampleData.toObject(for: Genres.self).genres
        homePresenter.fetchDatas()

        wait {
            expect(self.homePresenter.getGenreBy(0)).toNot(beNil())
        }
    }

    func testFetchDatas() {
        homePresenter.fetchDatas()

        wait {
            expect(self.homeInteractor.invokedDatasGetter).to(beTrue())
            expect(self.homeInteractor.invokedDatasGetterCount).to(equal(1))
            expect(self.homeView.invokedShowView).to(beTrue())
        }
    }

    func testGetItemCount() {
        expect(self.homePresenter.getItemCount).to(equal(0))
        homeInteractor.stubbedDatas = try! Bundle().getMockDataFor(key: "Genres").sampleData.toObject(for: Genres.self).genres
        homePresenter.fetchDatas()

        wait {
            expect(self.homePresenter.getItemCount).to(equal(19))
        }
    }

    func testGoToDiscover() {}
}

extension XCTestCase {
    func wait(interval: TimeInterval = 0.1, completion: @escaping (() -> Void)) {
        let exp = expectation(description: "")
        DispatchQueue.main.asyncAfter(deadline: .now() + interval) {
            completion()
            exp.fulfill()
        }
        waitForExpectations(timeout: interval + 0.1) // add 0.1 for sure `asyncAfter` called
    }
}
