//
//  MockReviewInteractor.swift
//  AssesMandiriTests
//
//  Created by Angga Setiawan on 10/02/24.
//

import Foundation
@testable import AssesMandiri

class MockReviewInteractor: ReviewInteractorInterface {

    var invokedDatasGetter = false
    var invokedDatasGetterCount = 0
    var stubbedDatas: AppPagedResponse<MovieReview>?

    func getReviews(params: AssesMandiri.MovieReviewRequest) async -> AssesMandiri.AppPagedResponse<AssesMandiri.MovieReview>? {
        invokedDatasGetter = true
        invokedDatasGetterCount = 1
        return stubbedDatas
    }
}
