//
//  ReviewTests.swift
//  AssesMandiriTests
//
//  Created by Angga Setiawan on 10/02/24.
//

import Nimble
import XCTest

import Foundation
@testable import AssesMandiri

// swiftlint:disable force_try discouraged_direct_init
class ReviewTests: XCTestCase {
    private var reviewInteractor: MockReviewInteractor!
    private var reviewRouter: MockReviewRouter!
    private var reviewView: MockReviewView!
    private var reviewPresenter: ReviewPresenterInterface!

    override func setUpWithError() throws {
        try super.setUpWithError()
        reviewInteractor = MockReviewInteractor()
        reviewRouter = MockReviewRouter()
        reviewView = MockReviewView()
        reviewPresenter = ReviewPresenter(view: reviewView, router: reviewRouter, interactor: reviewInteractor)
    }

    override func tearDownWithError() throws {
        try super.tearDownWithError()
        reviewPresenter = nil
        reviewInteractor = nil
        reviewRouter = nil
        reviewView = nil
    }

    func testNotifyViewDidLoad() {
        reviewPresenter.notifyViewDidLoad()
        expect(self.reviewView.invokedSetupView).to(beTrue())
        expect(self.reviewView.invokedSetTitle).to(beTrue())
        expect(self.reviewView.invokedTitle).to(equal("Reviews"))
    }

    func testFetchDatas() {
        reviewInteractor.stubbedDatas = try! Bundle().getMockDataFor(key: "Reviews").sampleData.toObject(for: AppPagedResponse<MovieReview>.self)

        reviewPresenter.movieId = 2
        reviewPresenter.fetchDatas()

        wait {
            expect(self.reviewInteractor.invokedDatasGetter).to(beTrue())
            expect(self.reviewInteractor.invokedDatasGetterCount).to(equal(1))
            expect(self.reviewView.invokedShowView).to(beTrue())
        }
    }

    func testGetReviewBy() {
        expect(self.reviewPresenter.getReviewBy(0)).to(beNil())

        reviewInteractor.stubbedDatas = try! Bundle().getMockDataFor(key: "Reviews").sampleData.toObject(for: AppPagedResponse<MovieReview>.self)

        reviewPresenter.movieId = 2
        reviewPresenter.fetchDatas()

        wait {
            expect(self.reviewPresenter.getReviewBy(0)).toNot(beNil())
        }
    }

    func testGetItemCount() {
        expect(self.reviewPresenter.getItemCount).to(equal(0))

        reviewInteractor.stubbedDatas = try! Bundle().getMockDataFor(key: "Reviews").sampleData.toObject(for: AppPagedResponse<MovieReview>.self)

        reviewPresenter.movieId = 2
        reviewPresenter.fetchDatas()

        wait {
            expect(self.reviewPresenter.getItemCount).toNot(equal(0))
        }
    }
}
