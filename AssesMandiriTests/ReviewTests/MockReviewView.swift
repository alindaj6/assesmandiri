//
//  MockReviewView.swift
//  AssesMandiriTests
//
//  Created by Angga Setiawan on 10/02/24.
//

import Foundation
@testable import AssesMandiri

class MockReviewView: ReviewViewInterface {
    var invokedSetupView = false
    var invokedSetTitle = false
    var invokedTitle = ""
    var invokedShowView = false

    func setupView() {
        invokedSetupView = true
    }

    func setTitle(with title: String) {
        invokedSetTitle = true
        invokedTitle = title
    }

    func showView() {
        invokedShowView = true
    }
}
