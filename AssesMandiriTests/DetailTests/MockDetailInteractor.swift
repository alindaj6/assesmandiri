//
//  MockDetailInteractor.swift
//  AssesMandiriTests
//
//  Created by Angga Setiawan on 10/02/24.
//

import Foundation
@testable import AssesMandiri

class MockDetailInteractor: DetailInteractorInterface {

    var invokedDatasDetailGetter = false
    var invokedDatasDetailGetterCount = 0
    var stubbedDatasDetail: MovieDetail?

    var invokedDatasCollectionGetter = false
    var invokedDatasCollectionGetterCount = 0
    var stubbedDatasCollection: MovieCollectionDetail?

    func getMovieDetail(request: AssesMandiri.MovieDetailRequest) async -> AssesMandiri.MovieDetail? {
        invokedDatasDetailGetter = true
        invokedDatasDetailGetterCount = 1
        return stubbedDatasDetail
    }

    func getCollection(id: Int) async -> AssesMandiri.MovieCollectionDetail? {
        invokedDatasCollectionGetter = true
        invokedDatasCollectionGetterCount = 1
        return stubbedDatasCollection
    }
}
