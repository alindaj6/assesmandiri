//
//  DetailTests.swift
//  AssesMandiriTests
//
//  Created by Angga Setiawan on 10/02/24.
//

import Nimble
import XCTest

import Foundation
@testable import AssesMandiri

// swiftlint:disable force_try discouraged_direct_init
class DetailTests: XCTestCase {
    private var detailInteractor: MockDetailInteractor!
    private var detailRouter: MockDetailRouter!
    private var detailView: MockDetailView!
    private var detailPresenter: DetailPresenterInterface!

    override func setUpWithError() throws {
        try super.setUpWithError()
        detailInteractor = MockDetailInteractor()
        detailRouter = MockDetailRouter()
        detailView = MockDetailView()
        detailPresenter = DetailPresenter(view: detailView, router: detailRouter, interactor: detailInteractor)
    }

    override func tearDownWithError() throws {
        try super.tearDownWithError()
        detailPresenter = nil
        detailInteractor = nil
        detailRouter = nil
        detailView = nil
    }

    func testNotifyViewDidLoad() {
        detailPresenter.notifyViewDidLoad()
        expect(self.detailView.invokedSetupView).to(beTrue())
        expect(self.detailView.invokedSetTitle).to(beTrue())
        expect(self.detailView.invokedTitle).to(equal("Movie"))
    }

    func testMovieId() {
        detailPresenter.movieId = 2
        expect(self.detailPresenter.movieId).to(equal(2))
    }

    func testFetchDatas() {
        expect(self.detailPresenter.movie).to(beNil())

        detailPresenter.movieId = 2

        let detail = try! Bundle().getMockDataFor(key: "Detail").sampleData.toObject(for: MovieDetail.self)
        detailInteractor.stubbedDatasDetail = detail

        let collection = try! Bundle().getMockDataFor(key: "Collection").sampleData.toObject(for: MovieCollectionDetail.self)
        detailInteractor.stubbedDatasCollection = collection

        detailPresenter.fetchDatas()

        wait {
            expect(self.detailInteractor.invokedDatasDetailGetter).to(beTrue())
            expect(self.detailInteractor.invokedDatasDetailGetterCount).to(equal(1))
            expect(self.detailInteractor.invokedDatasCollectionGetter).to(beTrue())
            expect(self.detailInteractor.invokedDatasCollectionGetterCount).to(equal(1))
            expect(self.detailView.invokedShowView).to(beTrue())
        }
    }

    func testMovie() {
        expect(self.detailPresenter.movie).to(beNil())

        detailPresenter.movieId = 2
        let detail = try! Bundle().getMockDataFor(key: "Detail").sampleData.toObject(for: MovieDetail.self)
        detailInteractor.stubbedDatasDetail = detail

        detailPresenter.fetchDatas()

        wait {
            expect(self.detailPresenter.movie).toNot(beNil())
        }
    }

    func testImageSources() {
        expect(self.detailPresenter.movie).to(beNil())
        expect(self.detailPresenter.imageSources).to(beEmpty())

        detailPresenter.movieId = 2
        let detail = try! Bundle().getMockDataFor(key: "Detail").sampleData.toObject(for: MovieDetail.self)
        detailInteractor.stubbedDatasDetail = detail

        detailPresenter.fetchDatas()

        wait {
            expect(self.detailPresenter.movie).toNot(beNil())
            expect(self.detailPresenter.imageSources).toNot(beEmpty())
        }
    }

    func testCollection() {
        expect(self.detailPresenter.movie).to(beNil())
        expect(self.detailPresenter.collection).to(beNil())

        detailPresenter.movieId = 2

        let detail = try! Bundle().getMockDataFor(key: "Detail").sampleData.toObject(for: MovieDetail.self)
        detailInteractor.stubbedDatasDetail = detail

        let collection = try! Bundle().getMockDataFor(key: "Collection").sampleData.toObject(for: MovieCollectionDetail.self)
        detailInteractor.stubbedDatasCollection = collection

        detailPresenter.fetchDatas()

        wait {
            expect(self.detailPresenter.movie).toNot(beNil())
            expect(self.detailPresenter.collection).toNot(beNil())
        }
    }

    func testCasts() {
        expect(self.detailPresenter.movie).to(beNil())
        expect(self.detailPresenter.casts).to(beEmpty())

        detailPresenter.movieId = 2

        let detail = try! Bundle().getMockDataFor(key: "Detail").sampleData.toObject(for: MovieDetail.self)
        detailInteractor.stubbedDatasDetail = detail

        detailPresenter.fetchDatas()

        wait {
            expect(self.detailPresenter.movie).toNot(beNil())
            expect(self.detailPresenter.casts).toNot(beEmpty())
        }
    }

    func testCrews() {
        expect(self.detailPresenter.movie).to(beNil())
        expect(self.detailPresenter.crews).to(beEmpty())

        detailPresenter.movieId = 2

        let detail = try! Bundle().getMockDataFor(key: "Detail").sampleData.toObject(for: MovieDetail.self)
        detailInteractor.stubbedDatasDetail = detail

        detailPresenter.fetchDatas()

        wait {
            expect(self.detailPresenter.movie).toNot(beNil())
            expect(self.detailPresenter.crews).toNot(beEmpty())
        }
    }

    func testReviews() {
        expect(self.detailPresenter.movie).to(beNil())
        expect(self.detailPresenter.reviews).to(beEmpty())

        detailPresenter.movieId = 2

        let detail = try! Bundle().getMockDataFor(key: "Detail").sampleData.toObject(for: MovieDetail.self)
        detailInteractor.stubbedDatasDetail = detail

        detailPresenter.fetchDatas()

        wait {
            expect(self.detailPresenter.movie).toNot(beNil())
            expect(self.detailPresenter.reviews).toNot(beEmpty())
        }
    }

    func testVideos() {
        expect(self.detailPresenter.movie).to(beNil())
        expect(self.detailPresenter.videos).to(beEmpty())

        detailPresenter.movieId = 2

        let detail = try! Bundle().getMockDataFor(key: "Detail").sampleData.toObject(for: MovieDetail.self)
        detailInteractor.stubbedDatasDetail = detail

        detailPresenter.fetchDatas()

        wait {
            expect(self.detailPresenter.movie).toNot(beNil())
            expect(self.detailPresenter.videos).toNot(beEmpty())
        }
    }

    func testGoToReview() {}
}
