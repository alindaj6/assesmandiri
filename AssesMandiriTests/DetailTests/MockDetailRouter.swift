//
//  MockDetailRouter.swift
//  AssesMandiriTests
//
//  Created by Angga Setiawan on 10/02/24.
//

import Foundation
@testable import AssesMandiri

class MockDetailRouter: DetailRouterInterface {
    func navigateToReview(from view: AssesMandiri.DetailViewInterface, movieId: Int) { }
}
