//
//  DiscoverTests.swift
//  AssesMandiriTests
//
//  Created by Angga Setiawan on 10/02/24.
//

import Nimble
import XCTest

import Foundation
@testable import AssesMandiri

// swiftlint:disable force_try discouraged_direct_init
class DiscoverTests: XCTestCase {
    private var discoverInteractor: MockDiscoverInteractor!
    private var discoverRouter: MockDiscoverRouter!
    private var discoverView: MockDiscoverView!
    private var discoverPresenter: DiscoverPresenterInterface!

    override func setUpWithError() throws {
        try super.setUpWithError()
        discoverInteractor = MockDiscoverInteractor()
        discoverRouter = MockDiscoverRouter()
        discoverView = MockDiscoverView()
        discoverPresenter = DiscoverPresenter(view: discoverView, router: discoverRouter, interactor: discoverInteractor)
    }

    override func tearDownWithError() throws {
        try super.tearDownWithError()
        discoverPresenter = nil
        discoverInteractor = nil
        discoverRouter = nil
        discoverView = nil
    }

    func testNotifyViewDidLoad() {
        discoverPresenter.notifyViewDidLoad()
        expect(self.discoverView.invokedSetupView).to(beTrue())
        expect(self.discoverView.invokedSetTitle).to(beTrue())
        expect(self.discoverView.invokedTitle).to(equal("Discover"))
    }

    func testGetMovieBy() {
        expect(self.discoverPresenter.getMovieBy(0)).to(beNil())
        let genres = try! Bundle().getMockDataFor(key: "Genres").sampleData.toObject(for: Genres.self).genres
        discoverPresenter.genre = genres.first
        discoverInteractor.stubbedDatas = try! Bundle().getMockDataFor(key: "Discovers").sampleData.toObject(for: AppPagedResponse<Movie>.self)
        discoverPresenter.fetchDatas()

        wait {
            expect(self.discoverPresenter.getMovieBy(0)).toNot(beNil())
        }
    }

    func testFetchDatas() {
        let genres = try! Bundle().getMockDataFor(key: "Genres").sampleData.toObject(for: Genres.self).genres
        discoverPresenter.genre = genres.first
        discoverInteractor.stubbedDatas = try! Bundle().getMockDataFor(key: "Discovers").sampleData.toObject(for: AppPagedResponse<Movie>.self)
        discoverPresenter.fetchDatas()

        wait {
            expect(self.discoverInteractor.invokedDatasGetter).to(beTrue())
            expect(self.discoverInteractor.invokedDatasGetterCount).to(equal(1))
            expect(self.discoverView.invokedShowView).to(beTrue())
        }
    }

    func testGetItemCount() {
        expect(self.discoverPresenter.getItemCount).to(equal(0))
        discoverInteractor.stubbedDatas = try! Bundle().getMockDataFor(key: "Discovers").sampleData.toObject(for: AppPagedResponse<Movie>.self)
        discoverPresenter.fetchDatas()

        wait {
            expect(self.discoverPresenter.getItemCount).to(equal(0))
        }
    }

    func testGoToDetail() {}
}
