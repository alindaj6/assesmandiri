//
//  MockDiscoverInteractor.swift
//  AssesMandiriTests
//
//  Created by Angga Setiawan on 10/02/24.
//

import Foundation
@testable import AssesMandiri

class MockDiscoverInteractor: DiscoverInteractorInterface {

    var invokedDatasGetter = false
    var invokedDatasGetterCount = 0
    var stubbedDatas: AppPagedResponse<Movie>?

    func getDiscovers(param: AssesMandiri.MovieDiscoverRequest) async -> AssesMandiri.AppPagedResponse<AssesMandiri.Movie>? {
        invokedDatasGetter = true
        invokedDatasGetterCount = 1
        return stubbedDatas
    }
}
