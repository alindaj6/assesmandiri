//
//  MockDiscoverRouter.swift
//  AssesMandiriTests
//
//  Created by Angga Setiawan on 10/02/24.
//

import Foundation
@testable import AssesMandiri

class MockDiscoverRouter: DiscoverRouterInterface {
    func navigateToDetail(from view: AssesMandiri.DiscoverViewInterface, movieId: Int) {}
}
